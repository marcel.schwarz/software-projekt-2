//
//  RoundTimer.swift
//  GoldWars
//
//  Created by Jakob Haag on 28.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import Foundation

class RoundTimer: Timer {
    
    var timer: Timer?
    var timeLeft: Int = 0
    var isHeartbeatLocked = false
    var calculate = false
    var roundEnded = "Syncing"
    
    func initTimer() {
        timer = Timer.scheduledTimer(
            timeInterval: 1.0,
            target: self,
            selector: #selector(onTimerFires),
            userInfo: nil,
            repeats: true
        )
    }
    
    func startTimer() {
        timeLeft = 30
    }
    
    func stopTimer() {
        guard timer != nil else { return }
        timer?.invalidate()
        timer = nil
    }
    
    func resumeTimer() {
        timer = Timer.scheduledTimer(
            timeInterval: 1.0,
            target: self,
            selector: #selector(onTimerFires),
            userInfo: nil,
            repeats: true
        )
    }
    
    @objc func onTimerFires()
    {
        timeLeft -= 1
        EntityManager.gameEMInstance.updateTime(time: (timeLeft > 0 ? String(timeLeft) : roundEnded))
        
        if timeLeft == 0 {
            EntityManager.gameEMInstance.removeModal()
            RoundCalculatorService.sharedInstance.resetNumberOfAttacksAndFormats()
            if !MultiplayerNetwork.sharedInstance.isSending {
                MultiplayerNetwork.sharedInstance.sendPlayerMoves(localRoundData: DataService.sharedInstance.localRoundData)
            }
            calculate = true
        }
        
        if timeLeft <= 0 {
            if calculate
                && !RoundCalculatorService.sharedInstance.isCalculating
                && DataService.sharedInstance.didReceiveAllData()
                && GameCenterManager.sharedInstance.isServer {
                RoundCalculatorService.sharedInstance.calculateRound()
                calculate = false
            }
        }
        
        if (!isHeartbeatLocked && (timeLeft % 7 == 0)){
            MultiplayerNetwork.sharedInstance.sendHeartbeatToPlayer()
            isHeartbeatLocked = true;
        }
    }
}
