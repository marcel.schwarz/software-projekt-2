//
//  AppDelegate.swift
//  GoldWars
//
//  Created by Aldin Duraki on 18.04.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import UIKit
import os

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let LOG = OSLog.init(subsystem: "AppDelegate", category: "AppDelegate")


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        os_log("application", log: LOG, type: .info)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        os_log("applicationWillResignActive", log: LOG, type: .debug)

    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        os_log("applicationDidEnterBackground", log: LOG, type: .debug)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "pauseGame"), object: nil)
        MultiplayerNetwork.sharedInstance.sendNotificationToPlayer(name: "pauseGame")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        os_log("applicationWillEnterForeground", log: LOG, type: .debug)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        os_log("applicationDidBecomeActive", log: LOG, type: .debug)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "resumeGame"), object: nil)
        MultiplayerNetwork.sharedInstance.sendNotificationToPlayer(name: "resumeGame")
    }
}

