//
//  SettingsScene.swift
//  GoldWars
//
//  Created by Tim Herbst on 10.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit

class SettingsScene: SKScene {
    
    var entityManager = EntityManager.settingsEMInstance
    var musicButtonText = ""
    var isMusicDeactivatedByUserDefault = UserDefaults.standard.bool(forKey: "noMusic")
    
    override func sceneDidLoad() {
        entityManager.setScene(scene: self)
        let positionX = self.size.width * 0.1
        let positionY = self.size.height * 0.05
        isMusicDeactivatedByUserDefault ? setMusicButtonTextByUserDefault(text: "Aus") : setMusicButtonTextByUserDefault(text: "An")
        entityManager.add(Button(name: "backToMenuScene",
                                 textureName: "yellow_button04",
                                 text: "Zurück",
                                 position: CGPoint(x: positionX, y: positionY),
                                 onButtonPress: {
                                    let scene = MenuScene(size: self.size)
                                    self.loadScene(scene: scene)
        }))
        entityManager.add(Button(name: "StopMenuMusic",
                                 textureName: "yellow_button04",
                                 text: musicButtonText,
                                 position: CGPoint(x: self.size.width * 0.6, y: self.size.height / 2),
                                 onButtonPress: {
                                    if SoundManager.sharedInstance.isMusicPlaying {
                                        SoundManager.sharedInstance.stopMenuMusic()
                                        SoundManager.sharedInstance.isMusicEnabled = false
                                        self.entityManager.changeSettingsButtonText(buttonName: "StopMenuMusic", text: "Aus")
                                    } else {
                                        SoundManager.sharedInstance.isMusicEnabled = true
                                        SoundManager.sharedInstance.startMenuMusic()
                                        self.entityManager.changeSettingsButtonText(buttonName: "StopMenuMusic", text: "An")
                                    }
        }))
        entityManager.add(Button(name: "StopMovingBackground",
                                 textureName: "yellow_button04",
                                 text: "An",
                                 position: CGPoint(x: self.size.width * 0.6, y: self.size.height / 2 - 100),
                                 onButtonPress: {
                                    if BackgroundComponent.isMovingBackgroundEnabled {
                                        BackgroundComponent.isMovingBackgroundEnabled = false
                                        self.entityManager.changeSettingsButtonText(buttonName: "StopMovingBackground", text: "Aus")
                                    } else {
                                        BackgroundComponent.isMovingBackgroundEnabled = true
                                        self.entityManager.changeSettingsButtonText(buttonName: "StopMovingBackground", text: "An")
                                    }
        }))
        entityManager.add(Label(fontnamed: "Courier-Bold",
                                name: "SettingsLabel",
                                text: "Einstellungen",
                                fontSize: 150.0,
                                fontColor: .black,
                                position: CGPoint(x: self.size.width * 0.5, y: self.size.height * 0.7),
                                horizontalAlignmentMode: .center,
                                vertikalAligmentMode: .baseline,
                                isAnimationEnabled: true,
                                isAnimationInfinite: true)
        )
        entityManager.add(Label(fontnamed: "Courier-Bold",
                                name: "LabelMusic",
                                text: "Sounds", fontSize: 50.0,
                                fontColor: .black,
                                position: CGPoint(x: self.size.width * 0.5, y: self.size.height / 2 - 15),
                                horizontalAlignmentMode: .right,
                                vertikalAligmentMode: .baseline,
                                isAnimationEnabled: true,
                                isAnimationInfinite: false)
        )
        entityManager.add(Label(fontnamed: "Courier-Bold",
                                name: "LabelBackground",
                                text: "Wind",
                                fontSize: 50.0,
                                fontColor: .black,
                                position: CGPoint(x: self.size.width * 0.5, y: self.size.height / 2 - 115),
                                horizontalAlignmentMode: .right,
                                vertikalAligmentMode: .baseline,
                                isAnimationEnabled: true,
                                isAnimationInfinite: false)
        )
        entityManager.add(Background(size: self.size))
    }
    
    func setMusicButtonTextByUserDefault(text: String) {
        self.musicButtonText = text
    }
    
    func loadScene(scene: SKScene) {
        let transition = SKTransition.flipVertical(withDuration: 0.5)
        entityManager.entities.removeAll()
        self.view?.presentScene(scene, transition: transition)
    }
    
    override func update(_ currentTime: TimeInterval) {
        if entityManager.entities.count != 0 {
            entityManager.getBackground()!.update(deltaTime: currentTime)
        }
    }
    
}
