//
//  Team.swift
//  GoldWars
//
//  Created by Jakob Haag on 02.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

enum Team: Int {
    case team1 = 1
    case team2 = 2

    static let allValues = [team1, team2]
}
