//
//  EloHelper.swift
//  GoldWars
//
//  Created by Marcel Schwarz on 13.06.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import GameKit
import os

struct EloDataForPeer : Codable {
    let scoreToReport: Int64
}

class EloHelper {
    
    static private let LOG = OSLog.init(subsystem: "EloHelper", category: "EloHelper")
    
    static let IDENTIFIER_ALL_ELO = "de.hft.stuttgart.ip2.goldwars.bestelo"
    static let IDENTIFIER_ELO = "de.hft.stuttgart.ip2.goldwars.matchmaking"
    
    static func updateEloScore(winner: GKPlayer, hatDenNikoGemacht looser: GKPlayer) {
            
        let leaderboard = GKLeaderboard.init(players: [winner, looser])
        leaderboard.identifier = EloHelper.IDENTIFIER_ELO
        
        leaderboard.loadScores{scores, error in
            
            // Get Scores
            let R_looser = scores?.filter { $0.player == looser }.first ?? GKScore(leaderboardIdentifier: EloHelper.IDENTIFIER_ELO, player: looser)
            let R_winner = scores?.filter { $0.player == winner }.first ?? GKScore(leaderboardIdentifier: EloHelper.IDENTIFIER_ELO, player: winner)
            
            // Calc ELO
            let Q_looser = pow(10.0, Double(R_looser.value) / 400.0)
            let Q_winner = pow(10.0, Double(R_winner.value) / 400.0)
            let E_looser = Q_looser / (Q_looser + Q_winner)
            let E_winner = Q_winner / (Q_looser + Q_winner)
        
            let R_winner_new = Int64(Double(R_winner.value) + 10.0 * (1.0 - E_winner))
            let R_looser_new = Int64(Double(R_looser.value) + 10.0 * (0.0 - E_looser))
            
            // Update Elo on leaderboard
            let winner_new = GKScore(leaderboardIdentifier: EloHelper.IDENTIFIER_ELO, player: winner)
            winner_new.value = R_winner_new
            let looser_new = GKScore(leaderboardIdentifier: EloHelper.IDENTIFIER_ELO, player: looser)
            looser_new.value = R_looser_new
            
            let scoreForPeer = winner == GameCenterManager.sharedInstance.localPlayer ? looser_new : winner_new
            let scoreForHost = winner == GameCenterManager.sharedInstance.localPlayer ? winner_new : looser_new
            
            MultiplayerNetwork.sharedInstance.sendEloData(scoreToReport: scoreForPeer)
            reportScore(score: scoreForHost.value)
        }
    }
    
    static func reportScore(score: Int64) {
        let gkScoreElo = GKScore(leaderboardIdentifier: EloHelper.IDENTIFIER_ELO, player: GKLocalPlayer.local)
        gkScoreElo.value = score
        let gkScoreAll = GKScore(leaderboardIdentifier: EloHelper.IDENTIFIER_ALL_ELO, player: GKLocalPlayer.local)
        gkScoreAll.value = score
        GKScore.report([gkScoreAll, gkScoreElo], withCompletionHandler: { error in
            if error != nil {
                os_log("Could not report %@", log: LOG, type: .error, error!.localizedDescription)
            } else {
                os_log("New Scores reported to EloSystem", log: self.LOG, type: .info)
            }
        })
    }
}
