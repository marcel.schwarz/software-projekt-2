//
//  Background.swift
//  GoldWars
//
//  Created by Niko Jochim on 02.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import GameplayKit

class Background: GKEntity {

    init(size: CGSize) {
        super.init()
        addComponent(BackgroundComponent(size: size))
    }

    override func update(deltaTime seconds: TimeInterval) {
         component(ofType: BackgroundComponent.self)?.update()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
