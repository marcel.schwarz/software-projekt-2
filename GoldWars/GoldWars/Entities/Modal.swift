//
//  Modal.swift
//  GoldWars
//
//  Created by Niko Jochim on 01.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import GameplayKit

enum ModalType: String {
    case BaseAttack
    case BaseMoveOwnUnits
    case PauseGame
    case QuitGame
}

class Modal: GKEntity{
    
    var entityManager = EntityManager.gameEMInstance
    var unitCount: Int
    var background: SKSpriteNode
    var closeButton: ButtonNode
    var header: SKLabelNode
    var body: SKLabelNode
    var footer: SKLabelNode
    var overlay: SKSpriteNode
    var type: ModalType
    
    init(modaltype: ModalType, base: Base?, anchorPoint: CGPoint, gameScene: SKScene, currentDraggedBase: Base?, touchLocation: CGPoint?, collisionBase: Base?) {
        self.type = modaltype
        unitCount = 0
        if base != nil {
            unitCount = base!.unitCount
        }
        
        
        let texture = SKTexture(imageNamed:"ModalBackground")
        background = SKSpriteNode(texture: texture, size: texture.size())
        background.setScale(2.4)
        background.position = anchorPoint
        background.zPosition = 4
        
        closeButton = ButtonNode(textureName: "red_cross", text: "", isEnabled: true, position: CGPoint(x: anchorPoint.x + 195, y: anchorPoint.y + 178), onButtonPress: {
            EntityManager.gameEMInstance.removeModal()
        })
        closeButton.size = CGSize(width: 26, height: 26)
        closeButton.zPosition = 5
        
        overlay = SKSpriteNode(color: UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.7), size: gameScene.size)
        overlay.anchorPoint = gameScene.anchorPoint
        overlay.zPosition = 3
        
        switch modaltype {
            case .BaseAttack:
                header = SKLabelNode(text: "Angriff")
                body = SKLabelNode(text: "\(unitCount / 2) Einheiten")
                footer = SKLabelNode()
            case .BaseMoveOwnUnits:
                header = SKLabelNode(text: "Formation")
                body = SKLabelNode(text: "\(unitCount / 2) Einheiten")
                footer = SKLabelNode()
            case .PauseGame:
                header = SKLabelNode(text: "Pause")
                body = SKLabelNode(text: "Warte auf Gegner...")
                footer = SKLabelNode()
                closeButton.zPosition = -1
            case .QuitGame:
                header = SKLabelNode(text: "Spiel verlassen")
                body = SKLabelNode(text: "Sicher verlassen?")
                footer = SKLabelNode()
        }
        
        self.header.position = CGPoint(x: anchorPoint.x, y: anchorPoint.y + 90)
        self.header.fontName = "HelveticaNeue-Bold"
        self.header.fontSize = 40
        self.header.zPosition = 5
        
        self.body.position = CGPoint(x: anchorPoint.x, y: anchorPoint.y + 15)
        self.body.numberOfLines = 2
        self.body.preferredMaxLayoutWidth = 390
        self.body.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        self.body.fontName = "HelveticaNeue-Bold"
        self.body.fontSize = 40
        self.body.zPosition = 5
        
        self.footer.position = CGPoint(x: anchorPoint.x, y: anchorPoint.y - 40)
        self.footer.fontName = "HelveticaNeue-Bold"
        self.footer.fontSize = 40
        self.footer.zPosition = 5
        
        super.init()
        
        switch modaltype {
            case .BaseAttack, .BaseMoveOwnUnits:
                let text = (modaltype == .BaseAttack) ? "Angriff" : "Senden"
                addComponent(SliderComponent(width: 300, position: CGPoint(x: anchorPoint.x , y: anchorPoint.y - 15)))
                addComponent(ButtonComponent(textureName: "yellow_button04", text: text, position: CGPoint(x: anchorPoint.x , y: anchorPoint.y - 95), isEnabled: true, onButtonPress: {
                    self.sendUnits(currentDraggedBase: currentDraggedBase, touchLocation: touchLocation!, gameScene: gameScene, collisionBase: collisionBase)
                    EntityManager.gameEMInstance.removeModal()
                }))
            case .QuitGame:
                addComponent(ButtonComponent(textureName: "yellow_button04", text: "Verlassen", position: CGPoint(x: anchorPoint.x , y: anchorPoint.y - 95), isEnabled: true, onButtonPress: {
                    EntityManager.gameEMInstance.removeModal()
                    GameCenterManager.sharedInstance.sendStateToPeers(state: State(state: 6))
                    GameCenterManager.sharedInstance.opponentQuit = false
                    GameCenterManager.sharedInstance.quitGame = true
                }))
            case .PauseGame:
                break
        }
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func removeModalEntities(gameScene: SKScene){
        for entity in entityManager.entities {
            if entityManager.isModal && entity.isMember(of: Modal.self) {
                entityManager.remove(entity)
            }
        }
    }
    
    func sendUnits(currentDraggedBase: Base?, touchLocation: CGPoint, gameScene: SKScene, collisionBase: Base?){
        for base in currentDraggedBase!.adjacencyList {
            if base == collisionBase  {
                RoundCalculatorService.sharedInstance.increaseMoveCounter(ownBase: currentDraggedBase?.ownershipPlayer == base.ownershipPlayer)
                entityManager.update((currentDraggedBase?.doPlayerMoveTypeToBase(base: base, units: Int(GameScene.sendUnits)))!)
                GameScene.sendUnits = 0
            }
        }
    }
}
