//
//  Way.swift
//  GoldWars
//
//  Created by Jakob Haag on 18.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class Way: GKEntity {

    var localWayComponent: SKShapeNode

    required init(fromBase: Base, toBase: Base) {
        
        let fromBaseX = fromBase.position.x
        let fromBaseY = fromBase.position.y
        let toBaseX = toBase.position.x
        let toBaseY = toBase.position.y
        
        let xControll1 = Way.computeX(x1: fromBaseX, x2: toBaseX, y1: fromBaseY, y2: toBaseY, factor: 2/5)
        let yControll1 = Way.computeY(x1: fromBaseX, x2: toBaseX, y1: fromBaseY, y2: toBaseY, factor: 2/5)
        let xControll2 = Way.computeX(x1: fromBaseX, x2: toBaseX, y1: fromBaseY, y2: toBaseY, factor: 3/5)
        let yControll2 = Way.computeY(x1: fromBaseX, x2: toBaseX, y1: fromBaseY, y2: toBaseY, factor: 3/5)

        let pathToDraw = CGMutablePath()
        pathToDraw.move(to: fromBase.position)
        pathToDraw.addCurve(
            to: toBase.position,
            control1: CGPoint(x: xControll1, y: yControll1),
            control2: CGPoint(x: xControll2, y: yControll2)
        )

        self.localWayComponent = SKShapeNode()
        self.localWayComponent.path = pathToDraw
        self.localWayComponent.strokeColor = UIColor(red: 0.852, green: 0.649, blue: 0.123, alpha: 1)
        self.localWayComponent.lineWidth = 10
        self.localWayComponent.zPosition = 0
        self.localWayComponent.name = "way"
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static var wayDelta: CGFloat = 5
    
    static func computeX(x1: CGFloat, x2: CGFloat, y1: CGFloat, y2: CGFloat, factor: CGFloat) -> CGFloat {
        let mix:CGFloat = Bool.random() ? -1/wayDelta : 1/wayDelta
        let x = x1 + factor * (x2 - x1) + mix * -(y2-y1)
        return x
    }
    
    static func computeY(x1: CGFloat, x2: CGFloat, y1: CGFloat, y2: CGFloat, factor: CGFloat) -> CGFloat {
        let mix:CGFloat = Bool.random() ? -1/wayDelta : 1/wayDelta
        let y = y1 + factor * (y2 - y1) + mix * (x2-x1)
        return y
    }
}
