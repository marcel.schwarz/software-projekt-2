//
//  Base.swift
//  GoldWars
//
//  Created by Marcel Schwarz on 18.04.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit
import GameplayKit
import GameKit

class Base: GKEntity{
    static var BASE_ID_COUNT: Int = 0
    var unitCount: Int
    var hasAttackBoost = false
    var hasDefenseBoost = false
    var adjacencyList: Array<Base>
    var changeOwnership: Bool
    var ownershipPlayer: GKPlayer?
    var baseID: Int
    var position: CGPoint
    
    init(position: CGPoint, player: GKPlayer! = nil, team: Team! = nil) {
        self.unitCount = 0
        self.adjacencyList = [Base]()
        self.changeOwnership = false
        self.ownershipPlayer = player
        self.baseID = Base.BASE_ID_COUNT
        Base.BASE_ID_COUNT += 1
        self.position = position
        super.init()
        
        let spritePos = position
        addComponent(DefaultBaseComponent(texture: SKTexture(imageNamed: "BaseTexture"), position: spritePos))
        if(team != nil && player != nil){
            addComponent(TeamComponent(team: team!, player: player!, position: spritePos))
            self.unitCount = 500
        }
        if ownershipPlayer == GKLocalPlayer.local {
            self.component(ofType: TeamComponent.self)?.unitcountLabel.text = "\(unitCount)"
        }
    }
    
    func doPlayerMoveTypeToBase(base: Base, units: Int) -> [GKEntity]{
        if base.ownershipPlayer != GKLocalPlayer.local {
            base.changeOwnership = true
        }
        self.unitCount -= units
        base.unitCount += units
        self.component(ofType: TeamComponent.self)?.unitcountLabel.text = "\(self.unitCount)"
        
        if base.component(ofType: TeamComponent.self)?.unitcountLabel.text != "" {
            if base.ownershipPlayer != self.ownershipPlayer {
                base.component(ofType: TeamComponent.self)?.unitcountLabel.text = "\(abs(base.unitCount - units * 2))"
            } else {
                base.component(ofType: TeamComponent.self)?.unitcountLabel.text = "\(base.unitCount)"
            }
        }
        DataService.sharedInstance.addMove(playerMove: PlayerMove(fromBase: self.baseID,
                                                                  toBase: base.baseID,
                                                                  unitCount: units)
        )
        return [self, base]
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
