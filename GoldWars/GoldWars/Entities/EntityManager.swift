//
//  EntityManager.swift
//  GoldWars
//
//  Created by Aldin Duraki on 01.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit
import GameplayKit
import GameKit

class EntityManager {
    
    static let gameEMInstance = EntityManager()
    static let menuEMInstance = EntityManager()
    static let settingsEMInstance = EntityManager()
    static let rulesEMInstance = EntityManager()
    
    var entities = Set<GKEntity>()
    var scene: SKScene
    var isModal: Bool
    
    private init() {
        isModal = false
        scene = SKScene.init()
    }
    
    func setScene(scene: SKScene){
        self.scene = scene
    }
    
    
    func add(_ entity: GKEntity) {
        entities.insert(entity)
        
        if let modalEntitiy = entity as? Modal {
            scene.addChild(modalEntitiy.background)
            scene.addChild(modalEntitiy.closeButton)
            scene.addChild(modalEntitiy.header)
            scene.addChild(modalEntitiy.body)
            scene.addChild(modalEntitiy.footer)
            scene.addChild(modalEntitiy.overlay)
            isModal = true
        }
        
        if let hudEntitiy = entity as? HUD {
            scene.addChild(hudEntitiy.hostLabel)
            scene.addChild(hudEntitiy.hostUnitsLabel)
            scene.addChild(hudEntitiy.peerLabel)
            scene.addChild(hudEntitiy.peerUnitsLabel)
            scene.addChild(hudEntitiy.leaveGame)
            scene.addChild(hudEntitiy.defSkill)
            scene.addChild(hudEntitiy.atkSkill)
            scene.addChild(hudEntitiy.spySkill)
            scene.addChild(hudEntitiy.roundTimerLabel)
            scene.addChild(hudEntitiy.finishButton)
            scene.addChild(hudEntitiy.backgroundRoundCounter)
            scene.addChild(hudEntitiy.currentRoundLabel)
            scene.addChild(hudEntitiy.roundsLabel)
            scene.addChild(hudEntitiy.roundLabel)
            scene.addChild(hudEntitiy.blockWholeScreenPane)
        }
        
        if let spinningLogoEntity = entity as? SpinningLogoEntity {
            scene.addChild(spinningLogoEntity.spinningLogoNode)
            scene.addChild(spinningLogoEntity.goldLetteringNode)
            scene.addChild(spinningLogoEntity.warsLetteringNode)
        }
        
        if let wayEntity = entity as? Way {
            scene.addChild(wayEntity.localWayComponent)
        }
        
        if let spriteNode = entity.component(ofType: DefaultBaseComponent.self) {
            scene.addChild(spriteNode.spriteNode)
        }
        
        if let spriteNode = entity.component(ofType: TeamComponent.self) {
            scene.addChild(spriteNode.unitcountLabel)
            scene.addChild(spriteNode.fire)
        }
        
        if let buttonNode = entity.component(ofType: ButtonComponent.self)?.buttonNode {
            scene.addChild(buttonNode)
        }
        if let nodes = entity.component(ofType: BackgroundComponent.self)?.nodes {
            for node in nodes {
                scene.addChild(node)
            }
        }
        if let sliderNode = entity.component(ofType: SliderComponent.self)?.sliderNode {
            scene.addChild(sliderNode.hiddenKnob)
            scene.addChild(sliderNode.sliderLine)
            scene.addChild(sliderNode.silderKnob)
        }
        if let labelNode = entity.component(ofType: LabelComponent.self)?.labelNode {
            scene.addChild(labelNode)
        }
        
    }
    
    func remove(_ entity: GKEntity) {
        if let spriteNode = entity.component(ofType: DefaultBaseComponent.self)?.spriteNode {
            spriteNode.removeFromParent()
        }
        if let sliderNode = entity.component(ofType: SliderComponent.self)?.sliderNode {
            sliderNode.hiddenKnob.removeFromParent()
            sliderNode.sliderLine.removeFromParent()
            sliderNode.silderKnob.removeFromParent()
        }
        if let modalButton = entity.component(ofType: ButtonComponent.self) {
            modalButton.buttonNode.removeFromParent()
        }
        entities.remove(entity)
    }
    
    func update(_  entities: [GKEntity]){
        for entity in entities {
            self.entities.update(with: entity)
            let base = (entity as! Base)
            
            if base.changeOwnership {
                if (entity.component(ofType: TeamComponent.self) != nil) {
                    entity.component(ofType: TeamComponent.self)!.change(to: (entities[0] as! Base).component(ofType: TeamComponent.self)!.team, to: (entities[0] as! Base).component(ofType: TeamComponent.self)!.player)
                } else {
                    base.addComponent(TeamComponent(
                        team: (entities[0] as! Base).component(ofType: TeamComponent.self)!.team,
                        player: (entities[0] as! Base).component(ofType: TeamComponent.self)!.player,
                        position: (base.component(ofType: DefaultBaseComponent.self)?.spriteNode.position)!
                        )
                    )
                    if let spriteNode = entity.component(ofType: TeamComponent.self) {
                        scene.addChild(spriteNode.unitcountLabel)
                        scene.addChild(spriteNode.fire)
                    }
                    SoundManager.sharedInstance.playSoundEffect(pathToFile: "attack_base",fileExtension: "wav",volumeLevel: -3.0)
                }
                GameCenterManager.sharedInstance.addAchievementProgress(identifier: "de.hft.stuttgart.ip2.goldwars.capture.fifty.bases", increasePercentComplete: 2)
            }
        }
    }
    
    func updateSnapshotModel(snapshotModel: SnapshotModel) {
        let bases = entities.filter{$0 is Base}
        for entity in bases{
            let base = entity as! Base
            let snapBase = self.getSnapshotBaseById(baseId: base.baseID, snapshotModel: snapshotModel)
            var getOwnerBySnapBase: GKPlayer? = nil
            base.unitCount = snapBase.unitCount
            base.changeOwnership = false
            
            if snapBase.ownership != nil {
                getOwnerBySnapBase = GameCenterManager.sharedInstance.getGKPlayerByUsername(displayName: snapBase.ownership!)
            } else {
                if entity.component(ofType: TeamComponent.self) != nil {
                    runExplosion(base: base)
                    entity.component(ofType: TeamComponent.self)!.fire.removeFromParent()
                    entity.component(ofType: TeamComponent.self)!.unitcountLabel.removeFromParent()
                    entity.removeComponent(ofType: TeamComponent.self)
                }
                base.ownershipPlayer = nil
            }
            if getOwnerBySnapBase != nil {
                if base.ownershipPlayer != getOwnerBySnapBase {
                    //TODO: Outsource following with a AnimationManager
                    runExplosion(base: base)
                }
                
                base.ownershipPlayer = getOwnerBySnapBase
                if entity.component(ofType: TeamComponent.self) != nil {
                    entity.component(ofType: TeamComponent.self)!.change(to: getTeamByPlayer(playerName: snapBase.ownership!), to: getOwnerBySnapBase!)
                } else {
                    entity.addComponent(TeamComponent(
                        team: getTeamByPlayer(playerName: snapBase.ownership!),
                        player: getOwnerBySnapBase!,
                        position: (entity.component(ofType: DefaultBaseComponent.self)?.spriteNode.position)!
                        )
                    )
                    if let spriteNode = entity.component(ofType: TeamComponent.self) {
                        scene.addChild(spriteNode.unitcountLabel)
                        scene.addChild(spriteNode.fire)
                    }
                }
                
                if getOwnerBySnapBase == GKLocalPlayer.local {
                    base.component(ofType: TeamComponent.self)?.unitcountLabel.text = "\(base.unitCount)"
                } else {
                    base.component(ofType: TeamComponent.self)?.unitcountLabel.text = ""
                }
            }
        }
        
        getHUD()!.updateUnitSum()
        
    }
    
    func runExplosion(base: Base) {
        let explosion = SKEmitterNode(fileNamed: "Explosion")!
        scene.addChild(explosion)
        explosion.zPosition = 2
        explosion.position = base.position
        explosion.name = "explosion"
        explosion.particleColorSequence = nil
        explosion.particleColorBlendFactor = 1.0
        let wait = SKAction.wait(forDuration: 1)
        let removeParticle = SKAction.removeFromParent()
        let sequence = SKAction.sequence([wait, removeParticle])
        explosion.run(sequence)
    }
    
    func getSnapshotBaseById(baseId: Int, snapshotModel: SnapshotModel) -> BaseEntityModel{
        return snapshotModel.baseEntites.filter { $0.baseId == baseId }[0]
    }
    
    func getBaseByPlayer(for player: GKPlayer) -> GKEntity? {
        for entity in entities {
            if let teamComponent = entity.component(ofType: TeamComponent.self),
                let _ = entity.component(ofType: DefaultBaseComponent.self) {
                if teamComponent.player == player {
                    return entity
                }
            }
        }
        return nil
    }
    
    func getBaseByTeam(for team: Team) -> GKEntity? {
        for entity in entities {
            if let teamComponent = entity.component(ofType: TeamComponent.self),
                let _ = entity.component(ofType: DefaultBaseComponent.self) {
                if teamComponent.team == team {
                    return entity
                }
            }
        }
        return nil
    }
    
    func getBasesByTeam(for team: Team) -> Set<Base> {
        return entities.filter{$0 is Base && ($0 as! Base).component(ofType: TeamComponent.self)?.team == team } as! Set<Base>
    }
    
    func getOpponentBases(for team: Team) -> Set<Base> {
        if(team == .team1){
            return getBasesByTeam(for: .team2)
        }else {
            return getBasesByTeam(for: .team1)
        }
    }
    
    func getBasesByPlayer(for player: GKPlayer) -> Set<Base> {
        return entities.filter{$0 is Base && ($0 as! Base).component(ofType: TeamComponent.self)?.player == player } as! Set<Base>
    }
    
    func getTeamByBase(base: Base) -> Team? {
        for entity in entities {
            if entity is Base && entity == base{
                for component in entity.components{
                    if component is TeamComponent {
                        return entity.component(ofType: TeamComponent.self)!.team
                    }
                }
            }
        }
        return nil
    }
    
    func getTeamByPlayer(playerName: String) -> Team {
        return entities.filter { $0 is Base && ($0 as! Base).component(ofType: TeamComponent.self)?.player.displayName == playerName }[0].component(ofType: TeamComponent.self)!.team
    }
    
    func getTeam() -> Team {
        return entities.filter { $0 is Base && ($0 as! Base).component(ofType: TeamComponent.self)?.player.displayName == GKLocalPlayer.local.displayName }[0].component(ofType: TeamComponent.self)!.team
    }
    
    func getBasebyID(id: Int) -> Base?{
        for entity in entities {
            if entity is Base && (entity as! Base).baseID == id {
                return (entity as! Base)
            }
        }
        return nil
    }
    
    func getBackground() -> GKEntity? {
        return entities.filter{$0 is Background}.first
    }
    
    func getBaseNodeByTeam(for team: Team) -> SKSpriteNode? {
        return getBaseByTeam(for: team)?.component(ofType: DefaultBaseComponent.self)?.spriteNode
    }
    
    func getButtonByName(buttonName:String) -> Button {
        return entities.filter{$0 is Button && ($0 as! Button).name == buttonName }[0] as! Button
    }
    
    func changeSettingsButtonText(buttonName: String, text: String) {
        let button = entities.filter{$0 is Button && ($0 as! Button).name == buttonName }[0] as! Button
        button.component(ofType: ButtonComponent.self)?.buttonNode.label.text = text
    }
    
    func getHUD() -> HUD? {
        return entities.filter{$0 is HUD}.first as? HUD
    }
    
    func getSnapshotModel() -> SnapshotModel {
        let bases = entities.filter{$0 is Base}
        var snapBase: [BaseEntityModel] = []
        
        for entity in bases {
            let base = entity as! Base
            snapBase.append(
                BaseEntityModel(
                    baseId: base.baseID,
                    unitCount: base.unitCount,
                    ownership: base.ownershipPlayer?.displayName,
                    hasAttackBoost: base.hasAttackBoost,
                    hasDefenceBoost: base.hasDefenseBoost
                )
            )
        }
        
        return SnapshotModel(baseEntites: snapBase)
    }
    
    func getUnitSum(by player: GKPlayer) -> Int{
        let bases = getBasesByPlayer(for: player)
        
        var sum = 0
        for base in bases {
            sum += base.unitCount
        }
        return sum
    }
    
    func removeModal() {
        entities.forEach({entity in
            if let modal = entity as? Modal {
                modal.background.removeFromParent()
                modal.closeButton.removeFromParent()
                modal.header.removeFromParent()
                modal.body.removeFromParent()
                modal.footer.removeFromParent()
                modal.overlay.removeFromParent()
                
                if let slider = modal.component(ofType: SliderComponent.self) {
                    slider.sliderNode.removeFromParent()
                    slider.sliderNode.hiddenKnob.removeFromParent()
                    slider.sliderNode.sliderLine.removeFromParent()
                }
                
                if let button = modal.component(ofType: ButtonComponent.self) {
                    button.buttonNode.removeFromParent()
                }
                self.remove(modal)
                isModal = false
            }
        })
    }
    
    func exitToMenu() {
        GameCenterManager.sharedInstance.reset()
        EntityManager.gameEMInstance.getTimer()?.stopTimer()
        RoundCalculatorService.sharedInstance.currentRound = 1
        EntityManager.gameEMInstance.scene.view?.presentScene(EntityManager.menuEMInstance.scene)
        EntityManager.gameEMInstance.entities.removeAll()
        EntityManager.gameEMInstance.scene.removeFromParent()
    }
    
    func getTimer() -> RoundTimer? {
        return getHUD()?.roundTimer
    }
    
    func updateTime(time: String) {
        getHUD()?.roundTimerLabel.text = time
    }
}
