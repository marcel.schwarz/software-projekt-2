//
//  Button.swift
//  GoldWars
//
//  Created by Niko Jochim on 02.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import GameKit

class Button: GKEntity{
    
    let name: String
    var isEnabled = true

    init(name: String, textureName: String, text: String, position: CGPoint, onButtonPress: @escaping () -> ()) {
        self.name = name
        super.init()
        self.addComponent(ButtonComponent(textureName: textureName, text: text, position: position, isEnabled: isEnabled, onButtonPress: onButtonPress))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
