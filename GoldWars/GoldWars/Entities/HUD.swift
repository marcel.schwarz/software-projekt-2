//
//  HUD.swift
//  GoldWars
//
//  Created by Niko Jochim on 01.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import GameplayKit
import GameKit

class HUD: GKEntity {
    
    var entityManager = EntityManager.gameEMInstance
    var hostLabel:SKLabelNode
    var hostUnitsLabel:SKLabelNode
    
    var peerLabel:SKLabelNode
    var peerUnitsLabel:SKLabelNode
    
    var leaveGame: ButtonNode
    
    var spySkill: SkillButtonNode
    var defSkill: SkillButtonNode
    var atkSkill: SkillButtonNode
    
    var roundTimerLabel: SKLabelNode
    let roundTimer: RoundTimer
    
    var backgroundRoundCounter: SKSpriteNode
    var currentRoundLabel: SKLabelNode
    var roundsLabel: SKLabelNode
    var roundLabel: SKLabelNode
    
    var finishButton: ButtonNode
    var blockWholeScreenPane: SKSpriteNode
    
    init(size: CGSize) {
        hostLabel = SKLabelNode(text: GameCenterManager.sharedInstance.hostingPlayer?.displayName)
        hostLabel.name = "hostLabel"
        hostUnitsLabel = SKLabelNode(text: "500" )
        peerLabel = SKLabelNode(text: GameCenterManager.sharedInstance.peerPlayer?.displayName)
        peerLabel.name = "peerLabel"
        peerUnitsLabel = SKLabelNode(text: "500")
        
        roundTimerLabel = SKLabelNode(text: "")
        roundTimerLabel.fontColor = UIColor.black
        roundTimerLabel.fontSize = CGFloat(45)
        roundTimerLabel.position = CGPoint(x: size.width * 0.5, y: size.height * 0.9)
        roundTimerLabel.zPosition = 900
        roundTimerLabel.horizontalAlignmentMode = .center
        
        self.roundTimer = RoundTimer()

        finishButton = SingeClickButtonNode(
            textureName: "finish_button",
            text: "",
            isEnabled: true,
            position: CGPoint(
                x: EntityManager.gameEMInstance.scene.size.width * 0.95 ,
                y: EntityManager.gameEMInstance.scene.size.height * 0.1),
            onButtonPress: { }
        )
        leaveGame = ButtonNode(textureName: "exitButton", text: "" , isEnabled: true, position: CGPoint(x: EntityManager.gameEMInstance.scene.size.width * 0.05, y: EntityManager.gameEMInstance.scene.size.height * 0.11), onButtonPress: {
                EntityManager.gameEMInstance.add(Modal(modaltype: .QuitGame, base: nil, anchorPoint: CGPoint(x: EntityManager.gameEMInstance.scene.size.width / 2 , y: EntityManager.gameEMInstance.scene.size.height / 2), gameScene: EntityManager.gameEMInstance.scene, currentDraggedBase: nil, touchLocation: nil, collisionBase: nil))
        })
        leaveGame.size = CGSize(width: 120, height: 120);

        defSkill = SkillButtonNode(
            textureName: "def_button",
            text: "Def",
            isEnabled: true,
            cooldown: 4,
            position: CGPoint(x: finishButton.position.x - 85, y: finishButton.position.y),
            onButtonPress: {
                DataService.sharedInstance.localRoundData.hasDefenceBoost = true
                GameCenterManager.sharedInstance.addAchievementProgress(identifier: "de.hft.stuttgart.ip2.goldwars.skill.first.time", increasePercentComplete: 100)
                GameCenterManager.sharedInstance.addAchievementProgress(identifier: "de.hft.stuttgart.ip2.goldwars.skill.def.ten", increasePercentComplete: 10)
                SoundManager.sharedInstance.playSoundEffect(pathToFile: "use_boost",fileExtension: "wav",volumeLevel: 0.0)
        }
        )
        spySkill = SkillButtonNode(
            textureName: "spy_button",
            text: "Spy",
            isEnabled: true,
            cooldown: 3,
            position: CGPoint(x: defSkill.position.x - 85, y: defSkill.position.y),
            onButtonPress: {
                EntityManager.gameEMInstance.getOpponentBases(for: EntityManager.gameEMInstance.getTeam()).forEach({base in base.component(ofType: TeamComponent.self)?.unitcountLabel.text = "\(base.unitCount)"})
                GameCenterManager.sharedInstance.addAchievementProgress(identifier: "de.hft.stuttgart.ip2.goldwars.skill.first.time", increasePercentComplete: 100)
                GameCenterManager.sharedInstance.addAchievementProgress(identifier: "de.hft.stuttgart.ip2.goldwars.skill.spy.ten", increasePercentComplete: 10)
                SoundManager.sharedInstance.playSoundEffect(pathToFile: "use_boost",fileExtension: "wav",volumeLevel: 0.0)
        }
        )
        atkSkill = SkillButtonNode(
            textureName: "atk_button",
            text: "Atk",
            isEnabled: true,
            cooldown: 4,
           position: CGPoint(x: spySkill.position.x - 85, y: spySkill.position.y),
            onButtonPress: {
                DataService.sharedInstance.localRoundData.hasAttackBoost = true
                GameCenterManager.sharedInstance.addAchievementProgress(identifier: "de.hft.stuttgart.ip2.goldwars.skill.first.time", increasePercentComplete: 100)
                GameCenterManager.sharedInstance.addAchievementProgress(identifier: "de.hft.stuttgart.ip2.goldwars.skill.atk.ten", increasePercentComplete: 10)
                SoundManager.sharedInstance.playSoundEffect(pathToFile: "use_boost",fileExtension: "wav",volumeLevel: 0.0)
        }
        )
        finishButton.size = CGSize(width: 100, height: 100)
        finishButton.zPosition = 2
        
        backgroundRoundCounter = SKSpriteNode(texture: SKTexture(imageNamed: "roundInfoTexture"))
        currentRoundLabel = SKLabelNode(fontNamed: "Courier-Bold")
        roundsLabel = SKLabelNode(fontNamed: "Courier-Bold")
        roundLabel = SKLabelNode(fontNamed: "Courier-Bold")
        
        blockWholeScreenPane = SKSpriteNode(color: UIColor.init(red: 0, green: 0, blue: 0, alpha: 0), size: size)
        blockWholeScreenPane.position = CGPoint(x: size.width * 0.5, y: size.height * 0.5)
        blockWholeScreenPane.zPosition = 899
        blockWholeScreenPane.isHidden = true
        super.init()
        initRoundInfo(size: size)
        finishButton.onButtonPress = { [unowned self] in
            self.finishRound()
        }
        
        hostLabel.position = CGPoint(x: size.width * 0.02, y: size.height * 0.95)
        hostLabel.horizontalAlignmentMode = .left
        peerLabel.position = CGPoint(x: size.width * 0.98, y: size.height * 0.95)
        peerLabel.horizontalAlignmentMode = .right
        hostUnitsLabel.position = CGPoint(x: size.width * 0.05, y: size.height * 0.9)
        peerUnitsLabel.position = CGPoint(x: size.width * 0.95, y: size.height * 0.9)
        setColor(labelNodes: [hostLabel,hostUnitsLabel,peerLabel,peerUnitsLabel])
        roundTimer.initTimer()
        startWithDuration()
    }
    
    func updateUnitSum(){
        hostUnitsLabel.text = "\(entityManager.getUnitSum(by: GameCenterManager.sharedInstance.hostingPlayer!))"
        peerUnitsLabel.text = "\(entityManager.getUnitSum(by: GameCenterManager.sharedInstance.peerPlayer!))"
    }
    
    func setColor(labelNodes: [SKLabelNode]) -> Void {
        for label in labelNodes {
            label.fontColor = SKColor.black
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func startWithDuration(){
        roundTimer.startTimer()
        finishButton.isEnabled = true
        self.roundTimer.roundEnded = "Berechnung"
        RoundCalculatorService.sharedInstance.isCalculating = false
        blockWholeScreenPane.isHidden = true
    }
    
    func finishRound() -> () {
        self.blockWholeScreenPane.isHidden = false
        self.roundTimer.timeLeft = 1;
        self.roundTimer.roundEnded = "Warte auf Gegner..."
    }
    
    func initRoundInfo(size: CGSize) -> () {
        backgroundRoundCounter.zPosition = 2
        backgroundRoundCounter.position = CGPoint(x: leaveGame.position.x + 63, y: leaveGame.position.y)
        backgroundRoundCounter.size = CGSize(width: 120, height: 120)
        currentRoundLabel.text = "\(RoundCalculatorService.sharedInstance.currentRound)"
        currentRoundLabel.fontSize = 50
        currentRoundLabel.fontColor = SKColor.black
        currentRoundLabel.verticalAlignmentMode = .center
        currentRoundLabel.position = CGPoint(x: backgroundRoundCounter.position.x, y: backgroundRoundCounter.position.y - 4)
        currentRoundLabel.zPosition = backgroundRoundCounter.zPosition + 1
        
        roundsLabel.zPosition = backgroundRoundCounter.zPosition + 1
        roundsLabel.text = "von \(RoundCalculatorService.sharedInstance.MAX_ROUNDS)"
        roundsLabel.fontColor = SKColor.black
        roundsLabel.verticalAlignmentMode = .center
        roundsLabel.fontSize = 12
        roundsLabel.position = CGPoint(x: currentRoundLabel.position.x, y: currentRoundLabel.position.y - 25)
        
        roundLabel.zPosition = backgroundRoundCounter.zPosition + 1
        roundLabel.text = "Runde"
        roundLabel.fontColor = SKColor.black
        roundLabel.verticalAlignmentMode = .center
        roundLabel.fontSize = 12
        roundLabel.position = CGPoint(x: currentRoundLabel.position.x, y: currentRoundLabel.position.y + 25)
    }
    
    func setCurrentRound(round: Int) -> Void {
        currentRoundLabel.text = "\(round)"
        let newRoundAction = SKAction.sequence([
            SKAction.scale(by: 2, duration: 1),
            SKAction.scale(by: 0.5, duration: 1),
        ])
        currentRoundLabel.run(newRoundAction)
        SoundManager.sharedInstance.playSoundEffect(pathToFile: "new_round", fileExtension: "wav", volumeLevel: 0.0)
        self.decreaseSkillRoundCooldownCounter()
    }
    
    func decreaseSkillRoundCooldownCounter() {
        self.spySkill.decreaseCooldown()
        self.atkSkill.decreaseCooldown()
        self.defSkill.decreaseCooldown()
    }
}
