//
//  SpinningLogoEntity.swift
//  GoldWars
//
//  Created by Niko Jochim on 15.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import Foundation
import GameplayKit

class SpinningLogoEntity : GKEntity {
    
    let spinningLogoNode: SpinningLogo3DNode
    let goldLetteringNode: SKSpriteNode
    let warsLetteringNode: SKSpriteNode
    
    init(sceneSize size: CGSize) {
        goldLetteringNode = SKSpriteNode(texture: SKTexture(imageNamed: "goldLettering"))
        goldLetteringNode.position = CGPoint(x: size.width / 2 , y: size.height * 0.83)
        goldLetteringNode.size = CGSize(width: size.width / 2.02, height: size.height / 3.6)
        
        warsLetteringNode = SKSpriteNode(texture: SKTexture(imageNamed: "warsLettering"))
        warsLetteringNode.position = CGPoint(x: size.width / 2, y: size.height * 0.17)
        warsLetteringNode.size = CGSize(width: size.width / 2.02, height: size.height / 3.6)
        
        spinningLogoNode = SpinningLogo3DNode()
        spinningLogoNode.viewportSize = CGSize(width: size.width / 5.12 * 0.69, height: size.height / 3.84 * 0.69)
        spinningLogoNode.zPosition = goldLetteringNode.zPosition - 1
        spinningLogoNode.position = CGPoint(x: size.width * 0.455, y: size.height * 0.83)
        
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
