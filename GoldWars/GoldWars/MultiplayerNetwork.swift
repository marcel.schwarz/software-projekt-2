//
//  MultiplayerNetwork.swift
//  GoldWars
//
//  Created by Niko Jochim on 10.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import GameplayKit
import Foundation
import GameKit

class MultiplayerNetwork{
    static let sharedInstance = MultiplayerNetwork()
    
    var isSending = false
    
    func sendData(data: Data) {
        let mmHelper = GameCenterManager.sharedInstance
        if let multiplayerMatch = mmHelper.myMatch {
            do {
                try multiplayerMatch.sendData(toAllPlayers: data, with: .reliable)
            } catch {
            }
        }
    }
    
    func sendDataToHost(data: Data) {
        
        if let multiplayerMatch = GameCenterManager.sharedInstance.myMatch{
            do {
                try multiplayerMatch.send(data, to: [GameCenterManager.sharedInstance.hostingPlayer!], dataMode: .reliable)
            } catch {
                //TODO: Add logging
            }
        }
    }
    
    func sendPlayerMoves(localRoundData: LocalRoundData) {
        if GameCenterManager.sharedInstance.isServer == false {
            self.isSending = true
            let encoder = JSONEncoder()
            let encoded = (try? encoder.encode(localRoundData))!
            sendDataToHost(data: encoded)
            DataService.sharedInstance.localRoundData.localPlayerMoves.removeAll()
            DataService.sharedInstance.localRoundData.hasAttackBoost = false
            DataService.sharedInstance.localRoundData.hasDefenceBoost = false
        }
    }
    
    func sendSnapshotModelToPlayers() {
        let encoder = JSONEncoder()
        let encoded = (try? encoder.encode(DataService.sharedInstance.snapshotModel))!
        sendData(data: encoded)
    }
    
    func sendMapModelToPlayers(mapModel: MapGenerationModel) {
        let encoder = JSONEncoder()
        let encoded = (try? encoder.encode(mapModel))!
        sendData(data: encoded)
    }
    
    func sendEloData(scoreToReport: GKScore) {
        let encoder = JSONEncoder()
        let encoded = (try? encoder.encode(EloDataForPeer(scoreToReport: scoreToReport.value)))!
        sendData(data: encoded)
    }
    
    func sendNotificationToPlayer(name: String) {
        let encoder = JSONEncoder()
        let encoded = (try? encoder.encode(NotificationModel(name: name)))!
        sendData(data: encoded)
    }
    
    func sendHeartbeatToPlayer() {
        let encoder = JSONEncoder()
        let encoded = (try? encoder.encode(Heartbeat(date: Date())))!
        sendData(data: encoded)
    }
}
