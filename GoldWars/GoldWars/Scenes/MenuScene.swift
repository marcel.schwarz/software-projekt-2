//
//  MenuScene.swift
//  GoldWars
//
//  Created by Aldin Duraki on 18.04.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit
import SceneKit
class MenuScene: SKScene {
    
    var entityManager = EntityManager.menuEMInstance
    
    override func sceneDidLoad() {
        GameCenterManager.sharedInstance.menusc = self
        entityManager.setScene(scene: self)
        let midX = self.size.width / 2
        let midY = self.size.height / 2
        entityManager.add(
            Button(name: "startGameButton",
                   textureName: "gold_button_3",
                   text: "Start Game",
                   position: CGPoint(x: midX, y: midY + 90),
                   onButtonPress: {
                    if CommandLine.arguments.contains("--no-matchmaking") {
                        self.loadScene(scene: GameScene(size: self.size))
                        SoundManager.sharedInstance.stopMenuMusic()
                    } else {
                        if GameCenterManager.isAuthenticated {
                            GameCenterManager.sharedInstance.presentMatchmaker()
                        } else {
                            GameCenterManager.sharedInstance.authUser()
                        }
                    }
                   }
            )
        )
        entityManager.add(
            Button(name: "settingsButton",
                   textureName: "gold_button_3",
                   text: "Einstellungen",
                   position: CGPoint(x: midX, y: midY ),
                   onButtonPress: { self.loadScene(scene: SettingsScene(size: self.size))}
            )
        )
        entityManager.add(
            Button(name: "gameCenterButton",
                   textureName: "gold_button_3",
                   text: "GameCenter",
                   position: CGPoint(x: midX, y: midY - 90),
                   onButtonPress: {
                    if GameCenterManager.isAuthenticated {
                        GameCenterManager.sharedInstance.presentGameCenter()
                    } else {
                        GameCenterManager.sharedInstance.authUser()
                    }
                   }
            )
        )
        entityManager.add(
            Button(name: "regelwerkButton",
                   textureName: "questionmark",
                   text: "",
                   position: CGPoint(x: self.size.width - 50, y: self.size.height - 50),
                   onButtonPress: { self.loadScene(scene: RulesScene(size: self.size)) }
            )
        )
        entityManager.add(Background(size: self.size))
        entityManager.add(SpinningLogoEntity(sceneSize: self.size))
        
        if SoundManager.sharedInstance.isMusicPlaying == false && SoundManager.sharedInstance.isMusicEnabled == true && !CommandLine.arguments.contains("--no-music") && !UserDefaults.standard.bool(forKey: "noMusic"){
            SoundManager.sharedInstance.startMenuMusic()
        }
    }
    
    func loadScene(scene: SKScene) {
        let transition = SKTransition.flipVertical(withDuration: 0.5)
        entityManager.entities.removeAll()
        self.view?.presentScene(scene, transition: transition)
    }
    
    override func update(_ currentTime: TimeInterval) {
        if entityManager.entities.count != 0 {
            entityManager.getBackground()!.update(deltaTime: currentTime)
            entityManager.getButtonByName(buttonName: "startGameButton").component(ofType: ButtonComponent.self)?.buttonNode.isEnabled = GameCenterManager.isAuthenticated
        }
        
        if GameCenterManager.sharedInstance.initIsFinish {
            self.loadScene(scene: GameCenterManager.sharedInstance.gameScene!)
        }
        
    }
}
