//
//  GameScene.swift
//  GoldWars
//
//  Created by Aldin Duraki on 18.04.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit
import GameplayKit
import GameKit

class GameScene: SKScene{
    
    var entityManager = EntityManager.gameEMInstance
    
    var isMoveTouch = false
    var currentDraggedBase : Base?
    static var sendUnits: Int = 0
    var collisionBase: Base?
    var gameEndEffects = false
    
    override func sceneDidLoad() {
        entityManager.setScene(scene: self)
        entityManager.add(HUD(size: self.size))
        entityManager.add(Background(size: self.size))
        if CommandLine.arguments.contains("--no-matchmaking") {
            _ = MapFactory(scene: self, entityManager: entityManager).load()
        }
        return
    }
    
    override func didMove(to view: SKView) {
        NotificationCenter.default.addObserver(self, selector: #selector(pauseGame), name: Notification.Name("pauseGame"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(resumeGame), name: Notification.Name("resumeGame"), object: nil)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        let touchLocation = touch.location(in: self)
        
        if isMoveTouch {
            isMoveTouch = false
            moveFireAndBase(base: currentDraggedBase!, touchLocation: currentDraggedBase!.position)
            
            addAttackDetails(touchLocation: touchLocation)
            
            entityManager.getBasesByPlayer(for: GKLocalPlayer.local).forEach({base in
                moveFireAndBase(base: base, touchLocation: base.position)
            })
        } else {
            for entity in entityManager.entities {
                if atPoint(touchLocation) == entity.component(ofType: DefaultBaseComponent.self)?.spriteNode {
                    entity.component(ofType: DefaultBaseComponent.self)?.spriteNode.touchesBegan(touches, with: event)
                }
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        let touchLocation = touch.location(in: self)
        
        for child in children {
            if atPoint(touchLocation) == child {
                child.touchesMoved(touches, with: event)
            }
        }
        checkSlider()
        let bases = entityManager.getBasesByPlayer(for: GKLocalPlayer.local)
        checkBases(bases: bases, touchLocation: touchLocation)
    }
    
    override func update(_ currentTime: TimeInterval) {
        entityManager.getBackground()?.update(deltaTime: currentTime)
        if GameCenterManager.sharedInstance.gameEnded && !gameEndEffects {
            gameEnd()
        }
        if GameCenterManager.sharedInstance.quitGame {
            gameQuit()
        }
    }
    
    
    func addAttackDetails(touchLocation: CGPoint){
        for base in currentDraggedBase!.adjacencyList {
            if atPoint(touchLocation) == base.component(ofType: DefaultBaseComponent.self)?.spriteNode {
                collisionBase = base
                // TODO: change interaction based on collision instead of touchlocation
                if currentDraggedBase!.unitCount > 1 {
                    if !checkIfMoveIsAble() {
                        return
                    }
                    let modaltype = setModalType()
                    entityManager.add(Modal(modaltype: modaltype,
                                            base: currentDraggedBase!,
                                            anchorPoint: CGPoint(x: self.size.width / 2 , y: self.size.height / 2),
                                            gameScene: self,
                                            currentDraggedBase: currentDraggedBase,
                                            touchLocation: touchLocation,
                                            collisionBase: collisionBase)
                    )
                    GameScene.sendUnits = Int(currentDraggedBase!.unitCount / 2)
                }
            }
        }
    }
    
    func checkSlider(){
        for e in entityManager.entities{
            if let modal = e as? Modal {
                if modal.type == ModalType.BaseAttack || modal.type == ModalType.BaseMoveOwnUnits {
                    GameScene.sendUnits = Int(((e.component(ofType: SliderComponent.self)?.sliderNode.getValue ?? 0) * CGFloat((e as! Modal).unitCount)).rounded(.up))
                    
                    //TODO: refactor this quick and dirty fix
                    if GameScene.sendUnits == 0 {
                        GameScene.sendUnits = 1
                    } else if Int(GameScene.sendUnits) == currentDraggedBase?.unitCount {
                        GameScene.sendUnits -= 1
                    }
                    modal.body.text = "\(GameScene.sendUnits) Einheiten "
                }
            }
        }
    }
    
    func gameEnd(){
        entityManager.getHUD()?.blockWholeScreenPane.isHidden = true
        gameEndEffects = true
        GameCenterManager.sharedInstance.gameEnded = false
        let move = SKAction.move(to: CGPoint(x: self.size.width / 2, y:  self.size.height / 2), duration: 1)
        let removeParticle = SKAction.removeFromParent()
        let sequence = SKAction.sequence([move, removeParticle])
        var actionAdded = false
        for nodeChild in children {
            if nodeChild.name == "peerLabel" ||  nodeChild.name == "hostLabel"{
                continue
            }
            if nodeChild.name == "way" {
                nodeChild.run(SKAction.removeFromParent())
                continue
            }
            if nodeChild.name != "clouds"{
                nodeChild.run(sequence) {
                    if !actionAdded {
                        let explosion = self.getFinalExplosion()
                        self.addChild(explosion)
                        let action = SKAction.afterDelay(2) {
                            (self.childNode(withName: "hostLabel") as! SKLabelNode).horizontalAlignmentMode = .center
                            (self.childNode(withName: "peerLabel") as! SKLabelNode).horizontalAlignmentMode = .center
                            self.childNode(withName: "hostLabel")?.run(SKAction.move(to: CGPoint(x: self.size.width * 0.25, y: self.size.height * 0.25), duration: 1))
                            self.childNode(withName: "peerLabel")?.run(SKAction.move(to: CGPoint(x: self.size.width * 0.75, y: self.size.height * 0.25), duration: 1))
                            self.initGameEndIcons()
                            let node = ButtonNode(textureName: "yellow_button05", text: "Menü", isEnabled: true, position: CGPoint(x: self.size.width / 2, y:  self.size.height / 2 - 300), onButtonPress: {
                                self.backToMenuAction()
                            })
                            node.name = "BackButton"
                            self.addChild(node)
                        }
                        explosion.run(action)
                        actionAdded = true
                    }
                }
            }
        }
    }
    
    func gameQuit() {
        entityManager.getHUD()?.blockWholeScreenPane.isHidden = true
        gameEndEffects = true
        GameCenterManager.sharedInstance.quitGame = false
        let move = SKAction.move(to: CGPoint(x: self.size.width / 2, y:  self.size.height / 2), duration: 1)
        let removeParticle = SKAction.removeFromParent()
        let sequence = SKAction.sequence([move, removeParticle])
        var actionAdded = false
        for nodeChild in children {
            if nodeChild.name == "peerLabel" ||  nodeChild.name == "hostLabel"{
                nodeChild.removeFromParent()
            }
            if nodeChild.name == "way" {
                nodeChild.run(SKAction.removeFromParent())
                continue
            }
            if nodeChild.name != "clouds"{
                nodeChild.run(sequence) {
                    if !actionAdded {
                        let explosion = self.getFinalExplosion()
                        self.addChild(explosion)
                        let action = SKAction.afterDelay(2) {

                        
                            let node = ButtonNode(textureName: "yellow_button05", text: "Menü", isEnabled: true, position: CGPoint(x: self.size.width / 2, y:  self.size.height / 2 - 300), onButtonPress: {
                                self.backToMenuAction()
                            })
                            node.name = "BackButton"
                            self.addChild(node)
                        }
                        explosion.run(action)
                        actionAdded = true
                    }
                }
            }
        }
        let disconnectLabel = SKLabelNode.init(fontNamed: "Courier-Bold")
        disconnectLabel.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        disconnectLabel.horizontalAlignmentMode = .center
        disconnectLabel.fontColor = UIColor.black
        disconnectLabel.alpha = 0
        disconnectLabel.setScale(0.1)
        if GameCenterManager.sharedInstance.opponentQuit {
            if GameCenterManager.sharedInstance.localPlayer == GameCenterManager.sharedInstance.hostingPlayer {
                disconnectLabel.text = GameCenterManager.sharedInstance.peerPlayer?.displayName
            } else {
                disconnectLabel.text = GameCenterManager.sharedInstance.hostingPlayer?.displayName
            }
            disconnectLabel.text?.append(" hat das Spiel verlassen!")
        } else {
            disconnectLabel.text = "Du hast das Spiel verlassen!"
        }
        let showUpAnimation = SKAction.sequence([
            SKAction.fadeAlpha(by: 1, duration: 2.5),
            SKAction.scale(by: 10, duration: 1),
        ])
        disconnectLabel.run(showUpAnimation)
        self.addChild(disconnectLabel)
    }
    
    func initGameEndIcons() {
        let iclonSize = CGSize(width: 400, height: 400)
        let winnerIcon = SKSpriteNode(texture: SKTexture(imageNamed: "winner"));
        winnerIcon.size = iclonSize
        let loserIcon = SKSpriteNode(texture: SKTexture(imageNamed: "BaseTexture"))
        loserIcon.size = iclonSize
        let loserFire = SKEmitterNode(fileNamed: "LoserFire")
        loserFire?.zPosition = loserIcon.zPosition - 1
        loserFire?.run(SKAction.scale(by: 4, duration: 8))
        let iconPosition1 = CGPoint(x: self.size.width * 0.25, y:  self.size.height * 0.55)
        let iconPosition2 = CGPoint(x: self.size.width * 0.75, y:  self.size.height * 0.55)
        let coin = SpinningLogo3DNode()
        if GameCenterManager.sharedInstance.winner == GameCenterManager.sharedInstance.hostingPlayer?.displayName {
            winnerIcon.position = iconPosition1
            coin.position = iconPosition1
            loserIcon.position = iconPosition2
            loserFire?.position = iconPosition2
        } else {
            winnerIcon.position = iconPosition2
            coin.position = iconPosition2
            loserIcon.position = iconPosition1
            loserFire?.position = iconPosition1
        }
        self.addChild(winnerIcon)
        self.addChild(coin)
        self.addChild(loserIcon)
        self.addChild(loserFire!)
    }
    
    func getFinalExplosion() -> SKEmitterNode {
        let explosion = SKEmitterNode(fileNamed: "Explosion")!
        explosion.zPosition = 2
        explosion.position = CGPoint(x: self.size.width / 2, y:  self.size.height / 2)
        explosion.name = "explosion"
        explosion.particleColorSequence = nil
        explosion.particleColorBlendFactor = 1.0
        return explosion
    }
    
    func backToMenuAction() {
        GameCenterManager.sharedInstance.reset()
        self.gameEndEffects = false
        entityManager.getTimer()?.stopTimer()
        RoundCalculatorService.sharedInstance.currentRound = 1
        self.view?.presentScene(MenuScene(size: self.size))
        entityManager.entities.removeAll()
        self.removeFromParent()
    }
    
    func checkBases(bases: Set<Base>, touchLocation: CGPoint){
        for base in bases {
            if atPoint(touchLocation) == base.component(ofType: DefaultBaseComponent.self)?.spriteNode {
                if !base.changeOwnership {
                    if !isMoveTouch {
                        currentDraggedBase = base
                    }
                    isMoveTouch = true
                    moveFireAndBase(base: base, touchLocation: touchLocation)
                    showNearestBases(base: base)
                }
            }
        }
    }
    
    func moveFireAndBase(base: Base, touchLocation: CGPoint){
        base.component(ofType: DefaultBaseComponent.self)?.spriteNode.position = touchLocation
        base.component(ofType: TeamComponent.self)?.fire.position = touchLocation
        base.component(ofType: TeamComponent.self)?.unitcountLabel.position = CGPoint(x:touchLocation.x + 30, y: touchLocation.y - 50)
    }
    
    func showNearestBases(base: Base){
        for adjacencyBase in base.adjacencyList {
            let node =  adjacencyBase.component(ofType: DefaultBaseComponent.self)?.spriteNode
            node?.run(SKAction.sequence([
                SKAction.resize(byWidth: 2, height: 2, duration: 0.5),
                SKAction.resize(byWidth: -2, height: -2, duration: 0.5)
            ]))
        }
    }
    
    func checkIfMoveIsAble() -> Bool{
        if isAttackMove() {
            return RoundCalculatorService.sharedInstance.numberOfAttacks < 2
        } else {
            return RoundCalculatorService.sharedInstance.numberOfOwnUnitMoves < 5
        }
    }
    
    func setModalType() -> ModalType {
        if isAttackMove() {
            return .BaseAttack
        } else {
            return .BaseMoveOwnUnits
        }
    }
    
    func isAttackMove() -> Bool {
        if collisionBase?.ownershipPlayer == nil {
            return true
        }
        return collisionBase?.ownershipPlayer != currentDraggedBase?.ownershipPlayer && !(collisionBase?.changeOwnership ?? false)
    }
    
    
    
    @objc func pauseGame() -> Void {
        if !self.gameEndEffects{
            entityManager.removeModal()
            entityManager.getHUD()?.roundTimer.stopTimer()
            entityManager.add(Modal(modaltype: .PauseGame,
                                    base: nil,
                                    anchorPoint: CGPoint(x: self.size.width / 2 , y: self.size.height / 2),
                                    gameScene: self,
                                    currentDraggedBase: nil,
                                    touchLocation: nil,
                                    collisionBase: nil
            ))
        }
    }
    
    @objc func resumeGame() -> Void {
        entityManager.removeModal()
        entityManager.getHUD()?.roundTimer.resumeTimer()
    }
    
}
