//
//  GameViewController.swift
//  GoldWars
//
//  Created by Aldin Duraki on 18.04.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
    var currentScene: SKView?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let view = self.view as! SKView? {
            let scene = MenuScene(size: self.view.bounds.size)
            EntityManager.menuEMInstance.setScene(scene: scene)
            scene.scaleMode = .aspectFill
            view.presentScene(scene)
            //TODO: create dev profile or remove on delivery
            view.showsFPS = true
            view.showsNodeCount = true
        }
        GameCenterManager.sharedInstance.viewController = self
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}
