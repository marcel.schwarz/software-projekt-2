//
//  LabelComponent.swift
//  GoldWars
//
//  Created by Tim Herbst on 13.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import GameplayKit

class LabelComponent: GKComponent {
    var labelNode: LabelNode
    
    init(fontnamed: String?, name: String, text: String, fontSize: CGFloat, fontColor: UIColor, position: CGPoint, horizontalAlignmentMode: SKLabelHorizontalAlignmentMode, vertikalAligmentMode: SKLabelVerticalAlignmentMode, isAnimationEnabled: Bool, isAnimationInfinite: Bool) {
        labelNode = LabelNode(fontNamed: fontnamed)
        labelNode.name = name
        labelNode.text = text
        labelNode.fontSize = fontSize
        labelNode.fontColor = fontColor
        labelNode.horizontalAlignmentMode = horizontalAlignmentMode
        labelNode.verticalAlignmentMode = vertikalAligmentMode
        labelNode.position = position
        if isAnimationEnabled {
            labelNode.sequentiallyBouncingZoom(delay: 0.3, infinite: isAnimationInfinite)
        }
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
