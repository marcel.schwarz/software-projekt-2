//
//  ButtonNode.swift
//  GoldWars
//
//  Created by Niko Jochim on 01.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit

class ButtonNode: SKSpriteNode {

    var isEnabled: Bool{
        didSet{
            if isEnabled {
                self.alpha = 1
                self.childNode(withName: "label")?.alpha = 1
            } else {
                self.alpha = 0.3
                self.childNode(withName: "label")?.alpha = 0.3
             }
        }
    }
    var label: SKLabelNode
    var onButtonPress: () -> ()
  
    init(textureName: String, text: String, isEnabled: Bool, position: CGPoint, onButtonPress: @escaping () -> ()) {
        self.onButtonPress = onButtonPress
        self.isEnabled = isEnabled
        let texture = SKTexture(imageNamed: textureName)
        
        label = SKLabelNode(fontNamed: "Courier-Bold")
        label.fontSize = 30
        label.fontColor = SKColor.black
        label.zPosition = 1
        label.verticalAlignmentMode = .center
        label.text = text
        label.name = "label"
        
        super.init(texture: texture, color: SKColor.white, size: texture.size())
        self.position = position
        
        self.addChild(label)
        isUserInteractionEnabled = true
  }
    
    func setTexture(textureName: String) {
        super.texture = SKTexture(imageNamed: textureName)
    }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    if isEnabled {
        let action = SKAction.sequence(
            [
                SKAction.scale(by: (3/4), duration: 0.3),
                SKAction.scale(by: (4/3), duration: 0.3),
            ]
        )

        self.childNode(withName: "label")?.run(action)
        self.run(action)
        onButtonPress()
    }
  }


  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

}
