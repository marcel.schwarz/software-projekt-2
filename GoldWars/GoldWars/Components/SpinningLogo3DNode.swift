//
//  SpinningLogo3DNode.swift
//  GoldWars
//
//  Created by Niko Jochim on 18.06.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import Foundation
import SpriteKit
import GameKit
class SpinningLogo3DNode : SK3DNode {
    
    init() {
          super.init(viewportSize: CGSize(width: 250, height: 250))
          let scnScene: SCNScene = {
              let scnScene = SCNScene()
              let cylinder = SCNCylinder(radius: 250, height: 50)
              let logoMaterial = SCNMaterial()
              let colorMaterial = SCNMaterial()
              logoMaterial.diffuse.contents = UIImage(named: "logo_no_background")
              colorMaterial.diffuse.contents = UIColor(red: 0.852, green: 0.649, blue: 0.123, alpha: 1)
              cylinder.materials = [colorMaterial,logoMaterial,logoMaterial]
              let cylinderNode = SCNNode(geometry: cylinder)
              cylinderNode.eulerAngles = SCNVector3(x: Float(CGFloat.pi / 2), y: 0, z: Float(CGFloat.pi / 2))
              let action = SCNAction.rotateBy(x: CGFloat(GLKMathDegreesToRadians(360)), y:0 , z: 0, duration: 8)
              let forever = SCNAction.repeatForever(action)
              cylinderNode.runAction(forever)
              scnScene.rootNode.addChildNode(cylinderNode)
              return scnScene
          }()
          self.scnScene = scnScene
          self.position = position
      }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

