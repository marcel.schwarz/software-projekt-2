//
//  SkillButtonNode.swift
//  GoldWars
//
//  Created by Simon Kellner on 17.06.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit

class SkillButtonNode: ButtonNode {
    
    
    var cooldownCounter = 0
    var hasCooled: Bool = false
    let cooldown: Int
    let displayText: String
    
    init(textureName: String, text: String, isEnabled: Bool, cooldown: Int, position: CGPoint, onButtonPress: @escaping () -> ()) {
        self.cooldown = cooldown
        self.displayText = text
        super.init(textureName: textureName, text: text, isEnabled: isEnabled, position: position, onButtonPress: onButtonPress)
        self.size = CGSize(width: 100, height: 100)
        self.label.fontSize = 25
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        super.isEnabled = false
        self.cooldownCounter = self.cooldown
        self.hasCooled = true
    }
    
    func decreaseCooldown() {
        if self.cooldownCounter <= 1 {
            super.isEnabled = true
            super.label.text = self.displayText
            let newRoundAction = SKAction.sequence([
                SKAction.scale(by: 1.5, duration: 1),
                SKAction.scale(by: 1/1.5, duration: 1),
            ])
            if hasCooled {
                self.run(newRoundAction)
                self.hasCooled = false
            }
        } else {
            super.isEnabled = false
            self.cooldownCounter -= 1
            super.label.text = self.cooldownCounter.description
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }
}
