//
//  SliderComponent.swift
//  GoldWars
//
//  Created by Marcel Schwarz on 24.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import GameplayKit


class SliderComponent: GKComponent {

    var sliderNode: SliderNode

    init(width: CGFloat, position: CGPoint) {
        sliderNode = SliderNode(width: width, position: position)
        sliderNode.zPosition = 5
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
