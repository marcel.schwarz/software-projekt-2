//
//  TeamComponent.swift
//  GoldWars
//
//  Created by Niko Jochim on 01.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit
import GameplayKit
import GameKit

class TeamComponent: GKComponent {
    var team: Team
    var player: GKPlayer
	let fire: SKEmitterNode
    var unitcountLabel : SKLabelNode

    init(team: Team, player: GKPlayer, position: CGPoint) {
		fire = SKEmitterNode(fileNamed: "Fire")!
		fire.zPosition = 1
		fire.position = position
		fire.name = "fire"
		fire.particleColorSequence = nil
		fire.particleColorBlendFactor = 1.0
    
        self.team = team
        self.player = player
        
        unitcountLabel = SKLabelNode(text: "")
        unitcountLabel.fontColor = SKColor.black
        unitcountLabel.horizontalAlignmentMode = .left
        unitcountLabel.verticalAlignmentMode = .center
        unitcountLabel.fontName = "AvenirNext-Bold"
        unitcountLabel.fontSize = 15
        unitcountLabel.position = CGPoint(x: position.x + 30 , y: position.y - 50 )
        unitcountLabel.zPosition = 3
        super.init()
        fire.particleColor = getColor(by: team)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getColor(by team: Team) -> SKColor {
        switch team {
            case .team1: return SKColor.red
            case .team2: return SKColor.purple
        }
    }
    
    func change(to team: Team, to player: GKPlayer) -> Void {
        self.team = team
        self.player = player
        self.fire.particleColor = getColor(by: team)
    }
}

