//
//  SkillButtonNode.swift
//  GoldWars
//
//  Created by Simon Kellner on 18.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit

class SingeClickButtonNode: ButtonNode {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.isEnabled = false
    }
    
}
