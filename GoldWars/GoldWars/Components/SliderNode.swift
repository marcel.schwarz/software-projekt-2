//
//  SliderNode.swift
//  GoldWars
//
//  Created by Niko Jochim on 05.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit


class SliderNode :SKNode {
    
    var sliderLine :SKShapeNode
    var hiddenKnob :SliderKnob
    var silderKnob :SKSpriteNode
    var width: CGFloat
    
    var getValue: CGFloat{
        get{
            silderKnob.position = hiddenKnob.position
            return ((hiddenKnob.position.x.rounded() - hiddenKnob.min) /  width)
        }
    }
    
    init(width: CGFloat, position: CGPoint) {
        self.width = width
        sliderLine = SKShapeNode(rectOf: CGSize(width: width, height: 8))
        sliderLine.position = position
        sliderLine.fillColor = SKColor.white
        sliderLine.zPosition = 4
        
        hiddenKnob = SliderKnob(circleOfRadius: 58 )
        hiddenKnob.min = position.x - width / 2
        hiddenKnob.max = position.x + width / 2
        hiddenKnob.fillColor = SKColor.red
        hiddenKnob.alpha = 0.001
        hiddenKnob.zPosition = sliderLine.zPosition + 1
        hiddenKnob.position = CGPoint(x: sliderLine.position.x, y: sliderLine.position.y + 1)
        
        silderKnob = SKSpriteNode(texture: SKTexture(imageNamed: "yellow_boxTick"))
        silderKnob.position = hiddenKnob.position
        silderKnob.zPosition = hiddenKnob.zPosition - 1
        silderKnob.size = CGSize(width: 50, height: 50)
        
        super.init()
        self.name = "slider"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class SliderKnob: SKShapeNode {
    var min = CGFloat()
    var max = CGFloat()
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches {
            let touchLocation = touch.location(in: self.scene!)
            
            if self.position.x >= min - 1 &&  self.position.x <= max + 1{
                self.position.x = touchLocation.x
            }
            
            if(self.position.x <= min){
                self.position.x = min
            }
            if(self.position.x >= max){
                self.position.x = max
                
            }
        }
    }
}

