//
//  BaseNode.swift
//  GoldWars
//
//  Created by Niko Jochim on 01.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit

class BaseNode: SKSpriteNode{

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		// TODO: PopUp Einheiten + Close PopUp
		self.run(
			SKAction.sequence(
                    [
                        SKAction.resize(byWidth: 20, height: 20, duration: 0.5),
                        SKAction.resize(byWidth: -20, height: -20, duration: 0.5)
                    ]
				)
			)
	}

	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		// TODO: zeige Angirff Effect
	}

	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		// TODO: Open Slider PopUp
	}


}
