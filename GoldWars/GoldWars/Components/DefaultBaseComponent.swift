//
//  DefaultBaseComponent.swift
//  GoldWars
//
//  Created by Niko Jochim on 01.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import GameplayKit
import SpriteKit
import GameKit

class DefaultBaseComponent: GKComponent {
    var spriteNode: BaseNode
    
    init(texture: SKTexture, position: CGPoint) {
        spriteNode = BaseNode(texture: texture, size: CGSize(width: 100, height: 100))
        spriteNode.position = position
        spriteNode.zPosition = 2
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
