//
//  ButtonComponent.swift
//  GoldWars
//
//  Created by Niko Jochim on 02.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import GameplayKit

class ButtonComponent: GKComponent {

    var buttonNode: ButtonNode

    init(textureName: String, text: String, position: CGPoint, isEnabled:Bool, onButtonPress: @escaping () -> ()) {
        buttonNode = ButtonNode(textureName: textureName,
                                text: text,
                                isEnabled: isEnabled,
                                position: position,
                                onButtonPress: onButtonPress)
        buttonNode.zPosition = 4
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
