//
//  BackgroundComponent.swift
//  GoldWars
//
//  Created by Niko Jochim on 02.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import GameplayKit

class BackgroundComponent: GKComponent{

    var nodes = [SKSpriteNode]()
    let size: CGSize
    static var isMovingBackgroundEnabled = true

    init(size: CGSize) {
        self.size = size
        for i in 0...2 {
            let sky = SKSpriteNode(imageNamed: "SkyBackground")
            sky.name = "clouds"
            sky.zPosition = -1
            sky.size = CGSize(width: size.width, height: size.height)
            sky.position = CGPoint(x: CGFloat(i) * sky.size.width, y: (size.height / 2))
            nodes.append(sky)
        }
        super.init()
   }

    func update(){
        if BackgroundComponent.isMovingBackgroundEnabled {
            for node in nodes{
                node.position.x -= 2
                if node.position.x < -(size.width) {
                    node.position.x += (size.width) * 3
                }
            }
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
