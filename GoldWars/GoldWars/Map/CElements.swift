//
//  CElemente.swift
//  GoldWars
//
//  Created by Jakob Haag on 18.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit

class CElement0: CenterElementProtocol {
    
    var bases: [Base] = []
    var centerBase: Base
    var ways: [Way] = []
    var id: Int = 0
    
    required init(frame: CGRect) {
        self.centerBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.midY
            )
        )
        self.bases.append(centerBase)
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return self.centerBase
    }
    
    func getRightConnection() -> Base? {
        return self.centerBase
    }
    
    func getBottomConnection() -> Base? {
        return self.centerBase
    }
    
    func getLeftConnection() -> Base? {
        return self.centerBase
    }
}

class CElement1: CenterElementProtocol {
    var bases: [Base] = []
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 1
    
    required init(frame: CGRect) {
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.midY
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.midY
            )
        )
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.rightBase)
        self.rightBase.adjacencyList.append(self.leftBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.leftBase, toBase: self.rightBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return self.leftBase
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return self.rightBase
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement2: CenterElementProtocol {
    var bases: [Base] = []
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 2
    
    required init(frame: CGRect) {
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.midY
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.midY
            )
        )
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.rightBase)
        self.rightBase.adjacencyList.append(self.leftBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.leftBase, toBase: self.rightBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return self.rightBase
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return self.leftBase
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement3: CenterElementProtocol {
    var bases: [Base] = []
    var centerBase: Base
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 3
    
    required init(frame: CGRect) {
        let delta = (frame.maxY - frame.minY) * 0.15
        self.centerBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.maxY - delta
            )
        )
        
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.minY + delta
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.minY + delta
            )
        )
        self.bases.append(centerBase)
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.rightBase)
        self.leftBase.adjacencyList.append(self.centerBase)
        self.rightBase.adjacencyList.append(self.leftBase)
        self.rightBase.adjacencyList.append(self.centerBase)
        self.centerBase.adjacencyList.append(self.leftBase)
        self.centerBase.adjacencyList.append(self.rightBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.leftBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.leftBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return self.leftBase
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return self.centerBase
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement4: CenterElementProtocol {
    var bases: [Base] = []
    var centerBase: Base
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 4
    
    required init(frame: CGRect) {
        let delta = (frame.maxY - frame.minY) * 0.15
        self.centerBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.maxY - delta
            )
        )
        
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.minY + delta
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.minY + delta
            )
        )
        self.bases.append(centerBase)
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.rightBase)
        self.leftBase.adjacencyList.append(self.centerBase)
        self.rightBase.adjacencyList.append(self.leftBase)
        self.rightBase.adjacencyList.append(self.centerBase)
        self.centerBase.adjacencyList.append(self.leftBase)
        self.centerBase.adjacencyList.append(self.rightBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.leftBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.leftBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return self.rightBase
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return self.centerBase
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement5: CenterElementProtocol {
    var bases: [Base] = []
    var centerBase: Base
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 5
    
    required init(frame: CGRect) {
        let delta = (frame.maxY - frame.minY) * 0.15
        self.centerBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.minY + delta
            )
        )
        
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.maxY - delta
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.maxY - delta
            )
        )
        self.bases.append(centerBase)
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.rightBase)
        self.leftBase.adjacencyList.append(self.centerBase)
        self.rightBase.adjacencyList.append(self.leftBase)
        self.rightBase.adjacencyList.append(self.centerBase)
        self.centerBase.adjacencyList.append(self.leftBase)
        self.centerBase.adjacencyList.append(self.rightBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.leftBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.leftBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return self.centerBase
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return self.leftBase
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement6: CenterElementProtocol {
    var bases: [Base] = []
    var centerBase: Base
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 6
    
    required init(frame: CGRect) {
        let delta = (frame.maxY - frame.minY) * 0.15
        self.centerBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.minY + delta
            )
        )
        
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.maxY - delta
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.maxY - delta
            )
        )
        self.bases.append(centerBase)
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.rightBase)
        self.leftBase.adjacencyList.append(self.centerBase)
        self.rightBase.adjacencyList.append(self.leftBase)
        self.rightBase.adjacencyList.append(self.centerBase)
        self.centerBase.adjacencyList.append(self.leftBase)
        self.centerBase.adjacencyList.append(self.rightBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.leftBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.leftBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return self.centerBase
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return self.rightBase
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement7: CenterElementProtocol {
    var bases: [Base] = []
    var topBase: Base
    var bottomBase: Base
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 7
    
    required init(frame: CGRect) {
        self.topBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.minY
            )
        )
        
        self.bottomBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.maxY
            )
        )
        
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.midY
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.midY
            )
        )
        self.bases.append(topBase)
        self.bases.append(bottomBase)
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.topBase)
        self.leftBase.adjacencyList.append(self.bottomBase)
        self.rightBase.adjacencyList.append(self.topBase)
        self.rightBase.adjacencyList.append(self.bottomBase)
        self.topBase.adjacencyList.append(self.leftBase)
        self.topBase.adjacencyList.append(self.rightBase)
        self.bottomBase.adjacencyList.append(self.rightBase)
        self.bottomBase.adjacencyList.append(self.leftBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.topBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.topBase, toBase: self.leftBase))
        ways.append(Way(fromBase: self.bottomBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.bottomBase, toBase: self.leftBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return self.topBase
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return self.bottomBase
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement8: CenterElementProtocol {
    var bases: [Base] = []
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 8
    
    required init(frame: CGRect) {
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.midY
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.midY
            )
        )
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.rightBase)
        self.rightBase.adjacencyList.append(self.leftBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.leftBase, toBase: self.rightBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return self.leftBase
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return nil
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement9: CenterElementProtocol {
    var bases: [Base] = []
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 9
    
    required init(frame: CGRect) {
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.midY
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.midY
            )
        )
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.rightBase)
        self.rightBase.adjacencyList.append(self.leftBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.leftBase, toBase: self.rightBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return nil
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return self.rightBase
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement10: CenterElementProtocol {
    var bases: [Base] = []
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 10
    
    required init(frame: CGRect) {
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.midY
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.midY
            )
        )
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.rightBase)
        self.rightBase.adjacencyList.append(self.leftBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.leftBase, toBase: self.rightBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return self.rightBase
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return nil
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement11: CenterElementProtocol {
    var bases: [Base] = []
    var centerBase: Base
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 11
    
    required init(frame: CGRect) {
        let delta = (frame.maxY - frame.minY) * 0.15
        self.centerBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.maxY - delta
            )
        )
        
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.minY + delta
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.minY + delta
            )
        )
        self.bases.append(centerBase)
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.rightBase)
        self.leftBase.adjacencyList.append(self.centerBase)
        self.rightBase.adjacencyList.append(self.leftBase)
        self.rightBase.adjacencyList.append(self.centerBase)
        self.centerBase.adjacencyList.append(self.leftBase)
        self.centerBase.adjacencyList.append(self.rightBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.leftBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.leftBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.rightBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return nil
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return self.centerBase
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement12: CenterElementProtocol {
    var bases: [Base] = []
    var centerBase: Base
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 12
    
    required init(frame: CGRect) {
        let delta = (frame.maxY - frame.minY) * 0.15
        self.centerBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.maxY - delta
            )
        )
        
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.minY + delta
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.minY + delta
            )
        )
        self.bases.append(centerBase)
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.rightBase)
        self.leftBase.adjacencyList.append(self.centerBase)
        self.rightBase.adjacencyList.append(self.leftBase)
        self.rightBase.adjacencyList.append(self.centerBase)
        self.centerBase.adjacencyList.append(self.leftBase)
        self.centerBase.adjacencyList.append(self.rightBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.leftBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.leftBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.rightBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return self.rightBase
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return nil
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement13: CenterElementProtocol {
    var bases: [Base] = []
    var centerBase: Base
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 13
    
    required init(frame: CGRect) {
        let delta = (frame.maxY - frame.minY) * 0.15
        self.centerBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.minY + delta
            )
        )
        
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.maxY - delta
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.maxY - delta
            )
        )
        self.bases.append(centerBase)
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.rightBase)
        self.leftBase.adjacencyList.append(self.centerBase)
        self.rightBase.adjacencyList.append(self.leftBase)
        self.rightBase.adjacencyList.append(self.centerBase)
        self.centerBase.adjacencyList.append(self.leftBase)
        self.centerBase.adjacencyList.append(self.rightBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.leftBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.leftBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.rightBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return nil
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return self.leftBase
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement14: CenterElementProtocol {
    var bases: [Base] = []
    var centerBase: Base
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 14
    
    required init(frame: CGRect) {
        let delta = (frame.maxY - frame.minY) * 0.15
        self.centerBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.minY + delta
            )
        )
        
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.maxY - delta
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.maxY - delta
            )
        )
        self.bases.append(centerBase)
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.rightBase)
        self.leftBase.adjacencyList.append(self.centerBase)
        self.rightBase.adjacencyList.append(self.leftBase)
        self.rightBase.adjacencyList.append(self.centerBase)
        self.centerBase.adjacencyList.append(self.leftBase)
        self.centerBase.adjacencyList.append(self.rightBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.leftBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.centerBase, toBase: self.leftBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return self.centerBase
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return nil
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement15: CenterElementProtocol {
    var bases: [Base] = []
    var topBase: Base
    var bottomBase: Base
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 15
    
    required init(frame: CGRect) {
        self.topBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.minY
            )
        )
        
        self.bottomBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.maxY
            )
        )
        
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.midY
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.midY
            )
        )
        self.bases.append(topBase)
        self.bases.append(bottomBase)
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.topBase)
        self.leftBase.adjacencyList.append(self.bottomBase)
        self.rightBase.adjacencyList.append(self.topBase)
        self.rightBase.adjacencyList.append(self.bottomBase)
        self.topBase.adjacencyList.append(self.leftBase)
        self.topBase.adjacencyList.append(self.rightBase)
        self.bottomBase.adjacencyList.append(self.rightBase)
        self.bottomBase.adjacencyList.append(self.leftBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.topBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.topBase, toBase: self.leftBase))
        ways.append(Way(fromBase: self.bottomBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.bottomBase, toBase: self.leftBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return self.topBase
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return nil
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement16: CenterElementProtocol {
    var bases: [Base] = []
    var topBase: Base
    var bottomBase: Base
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 16
    
    required init(frame: CGRect) {
        self.topBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.minY
            )
        )
        
        self.bottomBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.maxY
            )
        )
        
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.midY
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.midY
            )
        )
        self.bases.append(topBase)
        self.bases.append(bottomBase)
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.topBase)
        self.leftBase.adjacencyList.append(self.bottomBase)
        self.rightBase.adjacencyList.append(self.topBase)
        self.rightBase.adjacencyList.append(self.bottomBase)
        self.topBase.adjacencyList.append(self.leftBase)
        self.topBase.adjacencyList.append(self.rightBase)
        self.bottomBase.adjacencyList.append(self.rightBase)
        self.bottomBase.adjacencyList.append(self.leftBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.topBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.topBase, toBase: self.leftBase))
        ways.append(Way(fromBase: self.bottomBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.bottomBase, toBase: self.leftBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return nil
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return self.bottomBase
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}

class CElement17: CenterElementProtocol {
    var bases: [Base] = []
    var topBase: Base
    var bottomBase: Base
    var leftBase: Base
    var rightBase: Base
    var ways: [Way] = []
    var id: Int = 17
    
    required init(frame: CGRect) {
        self.topBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.minY
            )
        )
        
        self.bottomBase = Base(
            position: CGPoint(
                x: frame.midX,
                y: frame.maxY
            )
        )
        
        self.leftBase = Base(
            position: CGPoint(
                x: frame.minX,
                y: frame.midY
            )
        )
        
        self.rightBase = Base(
            position: CGPoint(
                x: frame.maxX,
                y: frame.midY
            )
        )
        self.bases.append(topBase)
        self.bases.append(bottomBase)
        self.bases.append(leftBase)
        self.bases.append(rightBase)
        
        self.connectInnerBases()
        self.createInternalWays()
    }
    
    func connectInnerBases() {
        self.leftBase.adjacencyList.append(self.topBase)
        self.leftBase.adjacencyList.append(self.bottomBase)
        self.rightBase.adjacencyList.append(self.topBase)
        self.rightBase.adjacencyList.append(self.bottomBase)
        self.topBase.adjacencyList.append(self.leftBase)
        self.topBase.adjacencyList.append(self.rightBase)
        self.bottomBase.adjacencyList.append(self.rightBase)
        self.bottomBase.adjacencyList.append(self.leftBase)
    }
    
    func createInternalWays() {
        ways.append(Way(fromBase: self.topBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.topBase, toBase: self.leftBase))
        ways.append(Way(fromBase: self.bottomBase, toBase: self.rightBase))
        ways.append(Way(fromBase: self.bottomBase, toBase: self.leftBase))
    }
    
    func getAllBases() -> [Base] {
        return self.bases
    }
    
    func getInternalWays() -> [Way] {
        return self.ways
    }
    
    func getTopConnection() -> Base? {
        return nil
    }
    
    func getRightConnection() -> Base? {
        return self.rightBase
    }
    
    func getBottomConnection() -> Base? {
        return nil
    }
    
    func getLeftConnection() -> Base? {
        return self.leftBase
    }
}
