//
//  TwoPlayerMapGenerator.swift
//  GoldWars
//
//  Created by Marcel Schwarz on 13.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import Foundation
import SpriteKit
import GameKit
import os

class MapFactory {
    
    let LOG = OSLog.init(subsystem: "MapGenerator", category: "MapFactory")
    
    var size: CGSize!
    var entityManager: EntityManager!
    
    required init(scene: SKScene, entityManager: EntityManager) {
        self.size = scene.size
        self.entityManager = entityManager
    }
    
    func load() -> MapGenerationModel {
        let mapModel = MapGenerationModel.new()
        load(fromModel: mapModel)
        return mapModel
    }
    
    func load(fromModel mapModel: MapGenerationModel) {
        os_log("Loading from TwoPlayerMapFactory", log: LOG, type: .info)
        
        // Generate bases structure
        os_log("Get player one base", log: LOG, type: .info)
        let basePlayerOne = Base(
            position: CGPoint(x: self.size.width * 0.07, y: self.size.height / 2),
            player: (GameCenterManager.sharedInstance.isServer) ? GKLocalPlayer.local : GameCenterManager.sharedInstance.myMatch?.players[0],
            team: .team1
        )
        
        os_log("Get player one's startbases", log: LOG, type: .info)
        var p1StartBases = [String: Base]()
        var ways = [Way]()
        
        if (mapModel.numBasesP1 == 1) {
            p1StartBases["mid"] = Base(position: CGPoint(x: self.size.width * 0.2, y: self.size.height * 0.5))
            ways.append(Way(fromBase: basePlayerOne, toBase: p1StartBases["mid"]!))
        } else if (mapModel.numBasesP1 == 2) {
            p1StartBases["top"] = Base(position: CGPoint(x: self.size.width * 0.2, y: self.size.height * 0.333))
            p1StartBases["bot"] = Base(position: CGPoint(x: self.size.width * 0.2, y: self.size.height * 0.666))
            ways.append(Way(fromBase: basePlayerOne, toBase: p1StartBases["top"]!))
            ways.append(Way(fromBase: basePlayerOne, toBase: p1StartBases["bot"]!))
        } else {
            p1StartBases["top"] = Base(position: CGPoint(x: self.size.width * 0.2, y: self.size.height * 0.25))
            p1StartBases["mid"] = Base(position: CGPoint(x: self.size.width * 0.2, y: self.size.height * 0.5))
            p1StartBases["bot"] = Base(position: CGPoint(x: self.size.width * 0.2, y: self.size.height * 0.75))
            ways.append(Way(fromBase: basePlayerOne, toBase: p1StartBases["top"]!))
            ways.append(Way(fromBase: basePlayerOne, toBase: p1StartBases["mid"]!))
            ways.append(Way(fromBase: basePlayerOne, toBase: p1StartBases["bot"]!))
        }
        
        os_log("Generate Map center", log: LOG, type: .info)
        let gridCellWidth = self.size.width * 0.2
        let gridCellHeight = self.size.height * 0.4
        let cellInsetX = CGFloat(self.size.width * 0.05)
        let cellInsetY = CGFloat(self.size.height * 0.07)
        
        let gridTopLeft = CGRect(
            x: self.size.width * 0.3,
            y: self.size.height * 0.1,
            width: gridCellWidth,
            height: gridCellHeight
        ).insetBy(dx: cellInsetX, dy: cellInsetY)
        
        let gridTopRight = CGRect(
            x: self.size.width * 0.5,
            y: self.size.height * 0.1,
            width: gridCellWidth,
            height: gridCellHeight
        ).insetBy(dx: cellInsetX, dy: cellInsetY)
        
        let gridBottomLeft = CGRect(
            x: self.size.width * 0.3,
            y: self.size.height * 0.5,
            width: gridCellWidth,
            height: gridCellHeight
        ).insetBy(dx: cellInsetX, dy: cellInsetY)
        
        let gridBottomRight = CGRect(
            x: self.size.width * 0.5,
            y: self.size.height * 0.5,
            width: gridCellWidth,
            height: gridCellHeight
        ).insetBy(dx: cellInsetX, dy: cellInsetY)
        
        let topLeft = CenterElementProvider.get(inFrame: gridTopLeft, withId: mapModel.topLeftId)
        let topRight = CenterElementProvider.get(inFrame: gridTopRight, withId: mapModel.topRightId)
        let bottomLeft = CenterElementProvider.get(inFrame: gridBottomLeft, withId: mapModel.bottomLeftId)
        let bottomRight = CenterElementProvider.get(inFrame: gridBottomRight, withId: mapModel.bottomRightId)
        
        ways.append(contentsOf: topLeft.getInternalWays())
        ways.append(contentsOf: topRight.getInternalWays())
        ways.append(contentsOf: bottomLeft.getInternalWays())
        ways.append(contentsOf: bottomRight.getInternalWays())
        
        os_log("Get player two base", log: LOG, type: .info)
        let basePlayerTwo = Base(
            position: CGPoint(x: self.size.width * 0.93, y: self.size.height / 2),
            player: (!GameCenterManager.sharedInstance.isServer) ? GKLocalPlayer.local : GameCenterManager.sharedInstance.myMatch?.players[0],
            team: .team2
        )
        
        os_log("Get player two's startbases", log: LOG, type: .info)
        var p2StartBases = [String: Base]()
        
        if (mapModel.numBasesP2 == 1) {
            p2StartBases["mid"] = Base(position: CGPoint(x: self.size.width * 0.8, y: self.size.height * 0.5))
            ways.append(Way(fromBase: basePlayerTwo, toBase: p2StartBases["mid"]!))
        } else if (mapModel.numBasesP2 == 2) {
            p2StartBases["top"] = Base(position: CGPoint(x: self.size.width * 0.8, y: self.size.height * 0.333))
            p2StartBases["bot"] = Base(position: CGPoint(x: self.size.width * 0.8, y: self.size.height * 0.666))
            ways.append(Way(fromBase: basePlayerTwo, toBase: p2StartBases["top"]!))
            ways.append(Way(fromBase: basePlayerTwo, toBase: p2StartBases["bot"]!))
        } else {
            p2StartBases["top"] = Base(position: CGPoint(x: self.size.width * 0.8, y: self.size.height * 0.25))
            p2StartBases["mid"] = Base(position: CGPoint(x: self.size.width * 0.8, y: self.size.height * 0.5))
            p2StartBases["bot"] = Base(position: CGPoint(x: self.size.width * 0.8, y: self.size.height * 0.75))
            ways.append(Way(fromBase: basePlayerTwo, toBase: p2StartBases["top"]!))
            ways.append(Way(fromBase: basePlayerTwo, toBase: p2StartBases["mid"]!))
            ways.append(Way(fromBase: basePlayerTwo, toBase: p2StartBases["bot"]!))
        }
        
        // Create adjacency mapping
        os_log("Connecting base p1 and start bases", log: LOG, type: .info)
        basePlayerOne.adjacencyList.append(contentsOf: p1StartBases.values)
        p1StartBases.values.forEach({base in base.adjacencyList.append(basePlayerOne)})
        
        if (p1StartBases["top"] != nil && topLeft.getLeftConnection() != nil) {
            os_log("Connecting p1 startbase top with topLeft segment", log: LOG, type: .info)
            p1StartBases["top"]!.adjacencyList.append(topLeft.getLeftConnection()!)
            topLeft.getLeftConnection()!.adjacencyList.append(p1StartBases["top"]!)
            ways.append(Way(fromBase: p1StartBases["top"]!, toBase: topLeft.getLeftConnection()!))
        }
        
        if (p1StartBases["mid"] != nil) {
            if (topLeft.getLeftConnection() != nil) {
                os_log("Connecting p1 startbase mid with topLeft segment", log: LOG, type: .info)
                p1StartBases["mid"]!.adjacencyList.append(topLeft.getLeftConnection()!)
                topLeft.getLeftConnection()!.adjacencyList.append(p1StartBases["mid"]!)
                ways.append(Way(fromBase: p1StartBases["mid"]!, toBase: topLeft.getLeftConnection()!))
            }
            
            if (bottomLeft.getLeftConnection() != nil) {
                os_log("Connecting p1 startbase mid with bottomLeft segment", log: LOG, type: .info)
                p1StartBases["mid"]!.adjacencyList.append(bottomLeft.getLeftConnection()!)
                bottomLeft.getLeftConnection()!.adjacencyList.append(p1StartBases["mid"]!)
                ways.append(Way(fromBase: p1StartBases["mid"]!, toBase: bottomLeft.getLeftConnection()!))
            }
        }
        
        if (p1StartBases["bot"] != nil && bottomLeft.getLeftConnection() != nil) {
            os_log("Connecting p1 startbase bot with bottomLeft segment", log: LOG, type: .info)
            p1StartBases["bot"]!.adjacencyList.append(bottomLeft.getLeftConnection()!)
            bottomLeft.getLeftConnection()!.adjacencyList.append(p1StartBases["bot"]!)
            ways.append(Way(fromBase: p1StartBases["bot"]!, toBase: bottomLeft.getLeftConnection()!))
        }
        
        if (topLeft.getRightConnection() != nil && topRight.getLeftConnection() != nil) {
            os_log("Connecting topLeft with topRight segment horizontally", log: LOG, type: .info)
            topLeft.getRightConnection()!.adjacencyList.append(topRight.getLeftConnection()!)
            topRight.getLeftConnection()!.adjacencyList.append(topLeft.getRightConnection()!)
            ways.append(Way(fromBase: topLeft.getRightConnection()!, toBase: topRight.getLeftConnection()!))
        }
        
        if (bottomLeft.getRightConnection() != nil && bottomRight.getLeftConnection() != nil) {
            os_log("Connecting bottomLeft with bottomRight segment horizontally", log: LOG, type: .info)
            bottomLeft.getRightConnection()!.adjacencyList.append(bottomRight.getLeftConnection()!)
            bottomRight.getLeftConnection()!.adjacencyList.append(bottomLeft.getRightConnection()!)
            ways.append(Way(fromBase: bottomLeft.getRightConnection()!, toBase: bottomRight.getLeftConnection()!))
        }
        
        if (topLeft.getBottomConnection() != nil && bottomLeft.getTopConnection() != nil) {
            os_log("Connecting topLeft with bottomLeft segment vertically", log: LOG, type: .info)
            topLeft.getBottomConnection()!.adjacencyList.append(bottomLeft.getTopConnection()!)
            bottomLeft.getTopConnection()!.adjacencyList.append(topLeft.getBottomConnection()!)
            ways.append(Way(fromBase: topLeft.getBottomConnection()!, toBase: bottomLeft.getTopConnection()!))
        }
        
        if (topRight.getBottomConnection() != nil && bottomRight.getTopConnection() != nil) {
            os_log("Connecting topRight with bottomRight segment vertically", log: LOG, type: .info)
            topRight.getBottomConnection()!.adjacencyList.append(bottomRight.getTopConnection()!)
            bottomRight.getTopConnection()!.adjacencyList.append(topRight.getBottomConnection()!)
            ways.append(Way(fromBase: topRight.getBottomConnection()!, toBase: bottomRight.getTopConnection()!))
        }
        
        os_log("Connecting base p2 and start bases", log: LOG, type: .info)
        basePlayerTwo.adjacencyList.append(contentsOf: p2StartBases.values)
        p2StartBases.values.forEach({base in base.adjacencyList.append(basePlayerTwo)})
        
        if (p2StartBases["top"] != nil && topRight.getRightConnection() != nil) {
            os_log("Connecting p2 startbase top with topRight segment", log: LOG, type: .info)
            p2StartBases["top"]!.adjacencyList.append(topRight.getRightConnection()!)
            topRight.getRightConnection()!.adjacencyList.append(p2StartBases["top"]!)
            ways.append(Way(fromBase: p2StartBases["top"]!, toBase: topRight.getRightConnection()!))
        }
        
        if (p2StartBases["mid"] != nil) {
            if (topRight.getRightConnection() != nil) {
                os_log("Connecting p2 startbase mid with topRight segment", log: LOG, type: .info)
                p2StartBases["mid"]!.adjacencyList.append(topRight.getRightConnection()!)
                topRight.getRightConnection()!.adjacencyList.append(p2StartBases["mid"]!)
                ways.append(Way(fromBase: topRight.getRightConnection()!, toBase: p2StartBases["mid"]!))
            }
            
            if (bottomRight.getRightConnection() != nil) {
                os_log("Connecting p2 startbase mid with bottomRight segment", log: LOG, type: .info)
                p2StartBases["mid"]!.adjacencyList.append(bottomRight.getRightConnection()!)
                bottomRight.getRightConnection()!.adjacencyList.append(p2StartBases["mid"]!)
                ways.append(Way(fromBase: p2StartBases["mid"]!, toBase: bottomRight.getRightConnection()!))
            }
        }
        
        if (p2StartBases["bot"] != nil && bottomRight.getRightConnection() != nil) {
            os_log("Connecting p2 startbase bot with bottomRight segment", log: LOG, type: .info)
            p2StartBases["bot"]!.adjacencyList.append(bottomRight.getRightConnection()!)
            bottomRight.getRightConnection()!.adjacencyList.append(p2StartBases["bot"]!)
            ways.append(Way(fromBase: p2StartBases["bot"]!, toBase: bottomRight.getRightConnection()!))
        }
        
        
        // Add all bases to the scene
        entityManager.add(basePlayerOne)
        p1StartBases.forEach({(key, base) in entityManager.add(base)})
        
        topLeft.getAllBases().forEach({base in entityManager.add(base)})
        topRight.getAllBases().forEach({base in entityManager.add(base)})
        bottomLeft.getAllBases().forEach({base in entityManager.add(base)})
        bottomRight.getAllBases().forEach({base in entityManager.add(base)})
        
        entityManager.add(basePlayerTwo)
        p2StartBases.forEach({(key, base) in entityManager.add(base)})
        
        ways.forEach({way in entityManager.add(way)})
    }
}
