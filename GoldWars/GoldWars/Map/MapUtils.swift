//
//  MapUtils.swift
//  GoldWars
//
//  Created by Marcel Schwarz on 29.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import GameKit
import os

protocol CenterElementProtocol {
    
    var bases: [Base] {get set}
    var ways: [Way] {get set}
    var id: Int { get }
    
    init(frame: CGRect)
    
    func getAllBases() -> [Base]
    func getInternalWays() -> [Way]
    func getTopConnection() -> Base?
    func getRightConnection() -> Base?
    func getBottomConnection() -> Base?
    func getLeftConnection() -> Base?
}

struct MapGenerationModel: Codable {
    
    let numBasesP1: Int
    let numBasesP2: Int
    let topLeftId: Int
    let topRightId: Int
    let bottomLeftId: Int
    let bottomRightId: Int
    
    static func new() -> MapGenerationModel {
        let noOfCElements = CenterElementProvider.centerElements.count - 1
        
        return MapGenerationModel(
            numBasesP1: Int.random(in: 1...3),
            numBasesP2: Int.random(in: 1...3),
            topLeftId: Int.random(in: 0...noOfCElements),
            topRightId: Int.random(in: 0...noOfCElements),
            bottomLeftId: Int.random(in: 0...noOfCElements),
            bottomRightId: Int.random(in: 0...noOfCElements)
        )
    }
    
}

class CenterElementProvider {
    static let LOG = OSLog.init(subsystem: "MapGenerator", category: "CenterElementProvider")
    
    static let centerElements: [CenterElementProtocol.Type] = [
        CElement0.self,
        CElement1.self,
        CElement2.self,
        CElement3.self,
        CElement4.self,
        CElement5.self,
        CElement6.self,
        CElement7.self,
        CElement8.self,
        CElement9.self,
        CElement10.self,
        CElement11.self,
        CElement12.self,
        CElement13.self,
        CElement14.self,
        CElement15.self,
        CElement16.self,
        CElement17.self
    ]
    
    static func get(inFrame frame: CGRect, withId id: Int) -> CenterElementProtocol {
        os_log("Getting new predifined center element from provider", log: LOG, type: .info)
        
        return centerElements[id].init(frame: frame)
    }
}
