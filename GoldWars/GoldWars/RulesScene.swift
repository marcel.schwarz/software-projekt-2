//
//  RulesScene.swift
//  GoldWars
//
//  Created by Chauntalle Schüle on 18.06.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit
import GameplayKit

class RulesScene: SKScene {
    
    var image: SKSpriteNode?
    
    override func sceneDidLoad() {
        EntityManager.rulesEMInstance.setScene(scene: self)
        EntityManager.rulesEMInstance.add(
            Button(name: "backToMenuScene",
                   textureName: "questionmark",
                   text: "",
                   position: CGPoint(x: self.size.width - 50, y: self.size.height - 50),
                   onButtonPress: {
                    EntityManager.rulesEMInstance.entities.removeAll()
                    self.view?.presentScene(
                        MenuScene(size: self.size),
                        transition: SKTransition.flipVertical(withDuration: 0.5)
                    )
                   }
            )
        )
        
        EntityManager.rulesEMInstance.add(
            Label(fontnamed: "Courier-Bold",
                  name: "rulesLabel",
                  text: "Regelwerk",
                  fontSize: 90,
                  fontColor: .black,
                  position: CGPoint(x: self.size.width * 0.5, y: self.size.height - 100),
                  horizontalAlignmentMode: .center,
                  vertikalAligmentMode: .baseline,
                  isAnimationEnabled: true,
                  isAnimationInfinite: true
            )
        )
        
        image = SKSpriteNode(imageNamed: "RulesSpiel")
        image!.size = CGSize(width: size.width * 0.8, height: size.height * 0.8)
        image!.position = CGPoint(x: size.width * 0.6, y: size.height * 0.6 - 150)
        image!.zPosition = 4
        
        self.addChild(image!)
        
        EntityManager.rulesEMInstance.add(
            Button(name: "Spiel",
                   textureName: "yellow_button13",
                   text: "Spiel",
                   position: CGPoint(x: self.size.width * 0.1, y: self.size.height * 0.5 + 150),
                   onButtonPress: { self.image!.texture = SKTexture(imageNamed: "RulesSpiel") }
            )
        )
        EntityManager.rulesEMInstance.add(
            Button(name: "Basen",
                   textureName: "yellow_button13",
                   text: "Basen",
                   position: CGPoint(x: self.size.width * 0.1, y: self.size.height * 0.5 + 85),
                   onButtonPress: { self.image!.texture = SKTexture(imageNamed: "RulesBasen") }
            )
        )
        EntityManager.rulesEMInstance.add(
            Button(name: "Einheiten",
                   textureName: "yellow_button13",
                   text: "Einheiten",
                   position: CGPoint(x: self.size.width * 0.1, y: self.size.height * 0.5 + 20),
                   onButtonPress: { self.image!.texture = SKTexture(imageNamed: "RulesEinheiten") }
            )
        )
        EntityManager.rulesEMInstance.add(
            Button(name: "Spielende",
                   textureName: "yellow_button13",
                   text: "Spielende",
                   position: CGPoint(x: self.size.width * 0.1, y: self.size.height * 0.5 - 45),
                   onButtonPress: { self.image!.texture = SKTexture(imageNamed: "RulesSpielende") }
            )
        )
        EntityManager.rulesEMInstance.add(
            Button(name: "Boost",
                   textureName: "yellow_button13",
                   text: "Boost",
                   position: CGPoint(x: self.size.width * 0.1, y: self.size.height * 0.5 - 110),
                   onButtonPress: { self.image!.texture = SKTexture(imageNamed: "RulesBoost") }
            )
        )
        EntityManager.rulesEMInstance.add(
            Button(name: "Erfolge",
                   textureName: "yellow_button13",
                   text: "Erfolge",
                   position: CGPoint(x: self.size.width * 0.1, y: self.size.height * 0.5 - 175),
                   onButtonPress: { self.image!.texture = SKTexture(imageNamed: "RulesErfolge") }
            )
        )
        EntityManager.rulesEMInstance.add(
            Button(name: "Credits",
                   textureName: "yellow_button13",
                   text: "Credits",
                   position: CGPoint(x: self.size.width * 0.1, y: self.size.height * 0.1 - 20),
                   onButtonPress: { self.image!.texture = SKTexture(imageNamed: "Credits") }
            )
        )
        EntityManager.rulesEMInstance.add(Background(size: self.size))
    }
    
    override func update(_ currentTime: TimeInterval) {
        if EntityManager.rulesEMInstance.entities.count != 0 {
            EntityManager.rulesEMInstance.getBackground()!.update(deltaTime: currentTime)
        }
    }
}
