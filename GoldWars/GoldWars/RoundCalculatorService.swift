//
//  RoundCalculatorService.swift
//  GoldWars
//
//  Created by Aldin Duraki on 13.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import GameKit
import os

class RoundCalculatorService {
    var entityManager = EntityManager.gameEMInstance
    static let sharedInstance = RoundCalculatorService()
    static let LOG = OSLog.init(subsystem: "Round Calculator", category: "RoundCalculatorService")
    
    var allPlayerMoves: [String: [PlayerMove]] = [:]
    // TODO: Better data structure
    var boosts: [String: (Bool, Bool)] = [:] // First bool is atk boost, second is def boost
    
    let ATK_BOOST_MULTIPLICATOR = 1.1
    let DEF_BOOST_MULTIPLICATOR = 1.1
    let MAX_ROUNDS = 20
    var currentRound = 1
    var isCalculating = false
    var numberOfAttacks = 0
    var numberOfOwnUnitMoves = 0
    
    func calculateRound() {
        os_log("Started calculating Round", log: RoundCalculatorService.LOG, type: .info)
        isCalculating = true
        let currentSnapshotModel = DataService.sharedInstance.snapshotModel
        
        var baseSpecificMoves = collectBaseSpecificMoves()
        
        // TODO: Refactor to a less complex way
        for (baseId, playerMovesByBase) in baseSpecificMoves {
            let targetBase = currentSnapshotModel?.baseEntites.filter { $0.baseId == baseId }[0]
            let possiblyOwnershipMoves = playerMovesByBase.filter { $0.key == targetBase?.ownership}
            
            for (playerName, playerMoves) in possiblyOwnershipMoves {
                for playerMove in playerMoves {
                    for base in currentSnapshotModel!.baseEntites {
                        if base.baseId == playerMove.fromBase {
                            base.unitCount -= playerMove.unitCount
                        }
                        if base.baseId == playerMove.toBase {
                            base.unitCount += playerMove.unitCount
                        }
                    }
                }
                baseSpecificMoves[baseId]!.removeValue(forKey: playerName)
            }
            
            for playerMoves in baseSpecificMoves[baseId]!.values {
                for playerMove in playerMoves {
                    for base in currentSnapshotModel!.baseEntites {
                        if base.baseId == playerMove.fromBase {
                            base.unitCount -= playerMove.unitCount
                        }
                    }
                }
            }
        }
        
        for (baseId, playerMovesByBase) in baseSpecificMoves {
            var combinePotentionalForces: [String: PlayerMove] = [:]
            
            for playerMoves in playerMovesByBase {
                combinePotentionalForces[playerMoves.key] = PlayerMove(
                    fromBase: playerMoves.value[0].fromBase,
                    toBase: playerMoves.value[0].toBase,
                    unitCount: 0
                )
                for move in playerMoves.value {
                    combinePotentionalForces[playerMoves.key]!.unitCount += move.unitCount
                }
            }
            
            if combinePotentionalForces.count > 0 {
                let sortedPotentionalCombinedForces = combinePotentionalForces.sorted { $0.1.unitCount > $1.1.unitCount }
                
                var playerMoveWithMaxUnits = sortedPotentionalCombinedForces[0]
                
                if playerMovesByBase.count >= 2 {
                    var playerMoveWithSecMaxUnits = sortedPotentionalCombinedForces[1]
                    if boosts[playerMoveWithMaxUnits.key]!.0 {
                        playerMoveWithMaxUnits.value.unitCount = Int(Double(playerMoveWithMaxUnits.value.unitCount) * ATK_BOOST_MULTIPLICATOR)
                    }
                    if boosts[playerMoveWithSecMaxUnits.key]!.0 {
                        playerMoveWithSecMaxUnits.value.unitCount = Int(Double(playerMoveWithSecMaxUnits.value.unitCount) * ATK_BOOST_MULTIPLICATOR)
                    }
                    
                    if playerMoveWithMaxUnits.value.unitCount < playerMoveWithSecMaxUnits.value.unitCount {
                        let temp = playerMoveWithMaxUnits
                        playerMoveWithMaxUnits = playerMoveWithSecMaxUnits
                        playerMoveWithSecMaxUnits = temp
                    }
                    
                    playerMoveWithMaxUnits.value.unitCount -= playerMoveWithSecMaxUnits.value.unitCount
                }
                
                for base in currentSnapshotModel!.baseEntites {
                    if base.baseId == playerMoveWithMaxUnits.value.toBase {
                        if base.ownership == nil {
                            base.unitCount += playerMoveWithMaxUnits.value.unitCount
                            base.ownership = playerMoveWithMaxUnits.value.unitCount == 0 ? nil : playerMoveWithMaxUnits.key
                        } else {
                            if boosts[base.ownership!]!.1 {
                                base.unitCount = Int(Double(base.unitCount) * DEF_BOOST_MULTIPLICATOR)
                            }
                            if base.unitCount < playerMoveWithMaxUnits.value.unitCount {
                                base.unitCount = playerMoveWithMaxUnits.value.unitCount - base.unitCount
                                base.ownership = playerMoveWithMaxUnits.key
                            } else if (base.unitCount == playerMoveWithMaxUnits.value.unitCount) {
                                base.ownership = nil
                                base.unitCount = 0
                            } else {
                                base.unitCount -= playerMoveWithMaxUnits.value.unitCount
                            }
                        }
                    }
                }
            }
            baseSpecificMoves.removeValue(forKey: baseId)
        }
        var player1BaseCount = 0;
        var player2BaseCount = 0;
        let player1 = GameCenterManager.sharedInstance.hostingPlayer?.displayName
        let player2 = GameCenterManager.sharedInstance.peerPlayer?.displayName
        for baseEntry in currentSnapshotModel!.baseEntites {
            if baseEntry.ownership == player1 {
                player1BaseCount += 1
            } else if baseEntry.ownership == player2 {
                player2BaseCount += 1
            }
        }
        currentSnapshotModel?.baseEntites = currentSnapshotModel!.baseEntites.map { (BaseEntityModel) -> BaseEntityModel in
            if BaseEntityModel.ownership == player1 {
                BaseEntityModel.unitCount += player1BaseCount
            } else if BaseEntityModel.ownership == player2 {
                BaseEntityModel.unitCount += player2BaseCount
            }
            return BaseEntityModel
        }
        allPlayerMoves.removeAll()
        DataService.sharedInstance.localRoundData.localPlayerMoves.removeAll()
        DataService.sharedInstance.localRoundData.hasAttackBoost = false
        DataService.sharedInstance.localRoundData.hasDefenceBoost = false
        
        if isGameOver() {
            let winner: String?
            if MAX_ROUNDS == currentRound {
                os_log("Game is over by rounds", log: RoundCalculatorService.LOG, type: .info)
                winner =  determineWinner(by: "rounds")
            } else {
                os_log("Game is over by capture", log: RoundCalculatorService.LOG, type: .info)
                winner =  determineWinner(by: "capture")
            }
            winner == GameCenterManager.sharedInstance.hostingPlayer?.displayName ? GameCenterManager.sharedInstance.sendStateToPeers(state: State(state: 4)) : GameCenterManager.sharedInstance.sendStateToPeers(state: State(state: 5))
            GameCenterManager.sharedInstance.winner = winner
            GameCenterManager.sharedInstance.gameEnded = true
            
            // Update EloSystem
            if winner == GameCenterManager.sharedInstance.hostingPlayer?.displayName {
                EloHelper.updateEloScore(winner: GameCenterManager.sharedInstance.hostingPlayer!, hatDenNikoGemacht: GameCenterManager.sharedInstance.peerPlayer!)
            } else {
                EloHelper.updateEloScore(winner: GameCenterManager.sharedInstance.peerPlayer!, hatDenNikoGemacht: GameCenterManager.sharedInstance.hostingPlayer!)
            }
            
            return
        }
        currentRound += 1
        entityManager.getHUD()?.setCurrentRound(round: currentRound)
        
        MultiplayerNetwork.sharedInstance.sendSnapshotModelToPlayers()
        DataService.sharedInstance.snapshotModel = currentSnapshotModel
        entityManager.updateSnapshotModel(snapshotModel: currentSnapshotModel!)
        entityManager.getHUD()?.startWithDuration()
        os_log("Finished calculating Round", log: RoundCalculatorService.LOG, type: .info)
    }
    
    func collectBaseSpecificMoves() -> [Int: [String: [PlayerMove]]] {
        for playerMove in DataService.sharedInstance.remotePlayerMoves {
            allPlayerMoves[playerMove.key] = playerMove.value.localPlayerMoves
        }
        
        boosts[GameCenterManager.sharedInstance.hostingPlayer!.displayName] = (
            DataService.sharedInstance.localRoundData.hasAttackBoost,
            DataService.sharedInstance.localRoundData.hasDefenceBoost
        )
        boosts[GameCenterManager.sharedInstance.peerPlayer!.displayName] = (
            DataService.sharedInstance.remotePlayerMoves[GameCenterManager.sharedInstance.peerPlayer!.displayName]?.hasAttackBoost ?? false,
            DataService.sharedInstance.remotePlayerMoves[GameCenterManager.sharedInstance.peerPlayer!.displayName]?.hasDefenceBoost ?? false
        )
        
        allPlayerMoves[GKLocalPlayer.local.displayName] = DataService.sharedInstance.localRoundData.localPlayerMoves
        
        var baseSpecificMoves: [Int: [String: [PlayerMove]]] = [:]
        
        for playerMove in allPlayerMoves {
            for move in playerMove.value {
                if baseSpecificMoves.keys.contains(move.toBase) {
                    if (baseSpecificMoves[move.toBase]?.keys.contains(playerMove.key))! {
                        baseSpecificMoves[move.toBase]?[playerMove.key]?.append(move)
                    } else {
                        baseSpecificMoves[move.toBase]?.merge([playerMove.key: [move]]){(current, _) in current}
                    }
                } else {
                    baseSpecificMoves[move.toBase] = [playerMove.key: [move]]
                }
            }
        }
        DataService.sharedInstance.remotePlayerMoves.removeAll()
        return baseSpecificMoves
    }
    
    func resolvePlayerMove(playerMove: PlayerMove, unitCount: Int, ownership: String?, resolveType: String) {
        //TODO: outsource playermoves
    }
    
    func resetNumberOfAttacksAndFormats() {
        self.numberOfAttacks = 0;
        self.numberOfOwnUnitMoves = 0;
    }
    
    func increaseMoveCounter(ownBase: Bool!) {
        if ownBase {
            self.numberOfOwnUnitMoves += 1
        } else {
            self.numberOfAttacks +=  1
        }
    }
    
    func isGameOver() -> Bool {
        let remoteplayerBasesCount = entityManager.getBasesByPlayer(for: GameCenterManager.sharedInstance.peerPlayer!).count
        let localplayerBasesCount = entityManager.getBasesByPlayer(for: GameCenterManager.sharedInstance.hostingPlayer!).count
        let onePlayerLoseAllBases = remoteplayerBasesCount == 0 || localplayerBasesCount == 0
        let reachMaxRounds = MAX_ROUNDS == currentRound
        return onePlayerLoseAllBases || reachMaxRounds
    }
    
    func determineWinner(by criteria: String) -> String {
        var winner: String?
        switch criteria {
        case "rounds":
            let peerPlayerBasesCount = entityManager.getBasesByPlayer(for: GameCenterManager.sharedInstance.peerPlayer!).count
            let hostingPlayerBasesCount = entityManager.getBasesByPlayer(for: GameCenterManager.sharedInstance.hostingPlayer!).count
            if peerPlayerBasesCount == hostingPlayerBasesCount {
                let hostingPlayerUnitCount = entityManager.getUnitSum(by: GameCenterManager.sharedInstance.hostingPlayer!)
                let peerPlayerUnitCount = entityManager.getUnitSum(by: GameCenterManager.sharedInstance.peerPlayer!)
                winner = hostingPlayerUnitCount > peerPlayerUnitCount ? GameCenterManager.sharedInstance.hostingPlayer?.displayName : GameCenterManager.sharedInstance.peerPlayer?.displayName
            } else {
                winner = hostingPlayerBasesCount > peerPlayerBasesCount ? GameCenterManager.sharedInstance.hostingPlayer?.displayName : GameCenterManager.sharedInstance.peerPlayer?.displayName
            }
        case "capture":
            winner = entityManager.getBasesByPlayer(for: GameCenterManager.sharedInstance.hostingPlayer!).count == 0 ? GameCenterManager.sharedInstance.peerPlayer?.displayName : GameCenterManager.sharedInstance.hostingPlayer?.displayName
        default:
            break
        }
        return winner!
    }
}
