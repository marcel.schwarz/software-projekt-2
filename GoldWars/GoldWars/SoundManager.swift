//
//  SoundManager.swift
//  GoldWars
//
//  Created by Tim Herbst on 10.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//

import SpriteKit
import AVFoundation
import os

class SoundManager {
    public static var sharedInstance = SoundManager()
    let LOG = OSLog.init(subsystem: "SoundManager", category: "SoundManager")
    
    var audioPlayer = AVAudioPlayer()
    var effectPlayer = AVAudioPlayer()
    var backgroundMainMenuAudio: URL?
    var soundEffect: URL?
    var isMusicPlaying = false
    var isMusicEnabled = true
    
    func startMenuMusic() {
        self.isMusicPlaying = true
        backgroundMainMenuAudio = Bundle.main.url(forResource: "intro-music", withExtension: "mp3")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: backgroundMainMenuAudio!)
        } catch {
            os_log("backgroundMusic is broken", log: LOG, type: .error)
        }
        audioPlayer.numberOfLoops = -1
        audioPlayer.prepareToPlay()
        if self.isMusicEnabled == true {
            audioPlayer.play()
            UserDefaults.standard.set(false, forKey: "noMusic")
        }
    }
    
    func playSoundEffect(pathToFile: String, fileExtension: String, volumeLevel: Float){
        soundEffect = Bundle.main.url(forResource: pathToFile, withExtension: fileExtension)
        do {
            effectPlayer = try AVAudioPlayer(contentsOf: soundEffect!)
        } catch {
            os_log("Could not load sound file %@", log: LOG, type: .error, pathToFile)
        }
        effectPlayer.volume += volumeLevel
        effectPlayer.prepareToPlay()
        if self.isMusicEnabled == true {
            effectPlayer.play()
        }
    }

    func stopMenuMusic() {
        audioPlayer.pause()
        self.isMusicPlaying = false
        UserDefaults.standard.set(true, forKey: "noMusic")
    }
    
    func setVolume(_ volume: Float) {
        audioPlayer.volume = volume
    }
    
    func getVolume() -> Float {
        return audioPlayer.volume
    }
}
