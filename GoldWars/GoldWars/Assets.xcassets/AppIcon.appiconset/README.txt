
The different image sizes are used for:

App Store:                     logo.png
iPad Pro - App Icon:           logo_167.png
iPad, iPad Mini - App Icon:    logo_152.png
iPad (all) - Spotlight:        logo_80.png
iPad (all) - Settings:         logo_58.png
iPad (all) - Notifications:    logo_40.png


Color Codes:

Coin:
    Main: #ae841a
    Edges: #daa520  (goldenrod)

Background flow:
    From: #a9003c
    To: #2f0011


The logo.svg file is a vectorized version of the coin without the background.