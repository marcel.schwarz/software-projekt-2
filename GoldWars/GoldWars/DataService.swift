//
//  PlayerMovesService.swift
//  GoldWars
//
//  Created by Tim Herbst on 13.05.20.
//  Copyright © 2020 SP2. All rights reserved.
//
import Foundation

struct Heartbeat: Codable {
    var date: Date
}

struct NotificationModel: Codable {
    let name: String
    
    init(name: String) {
        self.name = name
    }
}

struct PlayerMove: Codable {
    let fromBase: Int
    let toBase: Int
    var unitCount: Int
}

struct LocalRoundData: Codable {
    var localPlayerMoves: [PlayerMove]
    var hasAttackBoost: Bool
    var hasDefenceBoost: Bool
    
    init() {
        localPlayerMoves = []
        hasAttackBoost = false
        hasDefenceBoost = false
    }
}

class SnapshotModel: Codable {
    var baseEntites: [BaseEntityModel]

    init(baseEntites: [BaseEntityModel]) {
        self.baseEntites = baseEntites
    }
}

class BaseEntityModel: Codable {
    let baseId: Int
    var unitCount: Int
    var ownership: String?
    
    init(baseId: Int, unitCount: Int, ownership: String?, hasAttackBoost: Bool, hasDefenceBoost: Bool) {
        self.baseId = baseId
        self.unitCount = unitCount
        self.ownership = ownership
    }
}

class DataService {
    static let sharedInstance = DataService()
    var localRoundData: LocalRoundData = LocalRoundData()
    var remotePlayerMoves: [String: LocalRoundData] = [:]
    var snapshotModel: SnapshotModel?
    var hostingPlayer = GameCenterManager.sharedInstance.hostingPlayer
    
    var mapModel: MapGenerationModel?
    var entityManager = EntityManager.gameEMInstance
        
    func addMove(playerMove: PlayerMove) {
        let equalMoveIdx = localRoundData.localPlayerMoves.firstIndex(where: { (localPlayerMove) -> Bool in
            localPlayerMove.toBase == playerMove.toBase && localPlayerMove.fromBase == playerMove.fromBase
        })
        if equalMoveIdx != nil {
            localRoundData.localPlayerMoves[equalMoveIdx!].unitCount = Int(localRoundData.localPlayerMoves[equalMoveIdx!].unitCount) + Int(playerMove.unitCount)
        } else {
            self.localRoundData.localPlayerMoves.append(playerMove)
        }
    }
    
    func addRemotePlayerMoves(playerName: String, localRoundData: LocalRoundData) {
        self.remotePlayerMoves[playerName] = localRoundData
    }
    
    func didReceiveAllData() -> Bool {
        return remotePlayerMoves.count == GameCenterManager.sharedInstance.myMatch?.players.count
    }
    
    func setSnapshotModel(snapshotModel: SnapshotModel) {
        self.snapshotModel = snapshotModel
    }
    
    func setMapModel(model: MapGenerationModel) {
        self.mapModel = model
        MapFactory(scene: entityManager.scene, entityManager: entityManager).load(fromModel: DataService.sharedInstance.mapModel!)
    }
}
